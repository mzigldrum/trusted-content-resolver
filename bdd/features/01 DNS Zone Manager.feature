Feature:  Publishing the Trust Framework and the DID in the DNS Zone file

  Background: fully environment setup

    Given that the Notary Connector (API) is online
    And the DNS-Server is running (NSD & KNOT DNS-Server)
    And The DNS entry is configured
    And DNSSEC is configured

  Scenario: 00024-A1_A create request of trust framework is successfully reflected in the SQLite storage and DNS Zone file (200)

    Given the fully environment setup
    When the Notary has sent a create request of trust framework via the Notary Connector (API)
    And the Trust Framework has been created in the Trust List Provisioning Domain
    Then the Trust Framework is reflected as a PTR record in the DNS Zone Manager SQLite DataBase Zone file
    And the DID corresponding to the Trust Framework is published as URI records in the DNS Zone Manager SQLite DataBase Zone file

  Scenario: 00024-A1_2_An update request of trust framework is successfully reflected in the SQLite storage and DNS Zone File (200)

    Given the fully environment setup
    When the Notary has sent an update request of trust framework via the Notary Connector (API)
    And the updated Trust Framework has been published in the Trust List Provisioning Domain
    Then the Trust Framework update is reflected as a PTR record in the DNS Zone Manager SQLite DataBase Zone file
    And the DID corresponding to the Trust Framework is published as URI records in the DNS Zone Manager SQLite DataBase Zone file
    And the Zone file is resigned based on DNSSEC for every new update

  Scenario: 00024-A3_A wrong context leads to an exception (400)

    Given the fully environment setup
    When the context of the Notary request is wrong
    Then the request leads to an exception (400)
    And an audit entry is created

  Scenario: 00024-A4_A missing data leads to an exception (404)

    Given the fully environment setup
    When the Notary request has some missing data
    Then the request leads to an exception (404)
    And an audit entry is created

  Scenario: 00024-A5_An error is provided if a record is in progress by the operator
 #low priority
  @manual
    Given the fully environment setup
    And an update or create record is still in progress by the operator
    When a next create/update request of trust framework is sent
    Then an error `409 Conflict` is provided