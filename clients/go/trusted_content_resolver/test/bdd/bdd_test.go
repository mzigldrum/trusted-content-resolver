package bdd

import (
	"testing"

	"github.com/cucumber/godog"
)

func actorXGetsVerified() error {
	return godog.ErrPending
}

func anActorXToBeVerified() error {
	return godog.ErrPending
}

func weHaveXFSCTRAINTrustedContentResolverRESTAPI() error {
	return godog.ErrPending
}

func weInitiateAVerificationActorX() error {
	return godog.ErrPending
}

func TestFeatures(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario,
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"features"},
			TestingT: t, // Testing instance that will run subtests.
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run feature tests")
	}
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	ctx.Step(`^actor X gets verified$`, actorXGetsVerified)
	ctx.Step(`^an actor X to be verified$`, anActorXToBeVerified)
	ctx.Step(`^we have XFSC TRAIN Trusted Content Resolver REST API$`, weHaveXFSCTRAINTrustedContentResolverRESTAPI)
	ctx.Step(`^we initiate a verification actor X$`, weInitiateAVerificationActorX)
}
