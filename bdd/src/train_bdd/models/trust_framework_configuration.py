"""
Trust Framework Configuration Model
"""
from pydantic import BaseModel


class TrustFrameworkConfiguration(BaseModel):
    """
    The product MUST provide a Web UI which allows the administrator
    to add new trust frameworks and corresponding DIDs

    Developed by @Fraunhofer

    No code source yet available.
    """
    def create_did(self) -> str:
        """
        Create DID through TRAIN interfaces
        """
        return "some-did"
