"""
Spring Boot Actuator Base Model Interface
"""
from abc import ABC

import pydantic
import requests


class SpringBootActuator(pydantic.BaseModel, ABC):
    """
    Interface helper for test spring-boot-actuator based REST API
    """

    host: pydantic.HttpUrl

    def is_up(self) -> bool:
        """
        Expecting acknowledge from `actuator/health` endpoint.

        See `https://www.baeldung.com/spring-boot-actuators#6-health-groups`_
        """
        url = f"{self.host}actuator/health"
        print(f"HTTP GET {url}")
        try:
            response = requests.get(url, timeout=1)
        except requests.exceptions.ConnectionError as exc:
            print("Can not connect, please ensure server is up", exc)
            return False

        assert response.status_code == 200
        response_json = response.json()
        assert response_json['status'] == "UP"

        return True
