Feature: Trust List Management
  allow CRUD (create, read, update, delete) operations on the trust list at the Trusted Data Store

  Scenario: 00015-A1_A request update has been successfully reflected in the trust list (200)
    Given fully environment setup
      And a trust list at the Trusted Data Store
     When the Notary sends an update request of trust list via the Notary Connector (API)
     Then a request update is reflected in the trust list (200)

    When in create operation
    Then a new trust list entry is created

    When in read operation

    Then trust list is referenced by name `endpoint/federation1.test.train.trust-scheme.de`
    # e.g. https://tspa.trust-scheme.de/tspa_train_domain/api/v1/scheme/federation1.test.train.trust-scheme.de

    When in update operation
    Then the requested change is reflected in trust list

    When in delete operation
    Then the trust list entry of the entity is deleted from the list

    Given client rust client installed