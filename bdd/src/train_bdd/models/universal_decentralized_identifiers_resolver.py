"""
Universal Decentralized Identifiers Resolver Model
"""
import pydantic

from .._env import DID_RESOLVER_HOST
from ._spring_boot_actuator import SpringBootActuator


class UniversalDecentralizedIdentifiersResolver(SpringBootActuator):
    """
    Universal Decentralized Identifiers Resolver is a REST api.

    See `https://github.com/decentralized-identity/universal-resolver`_
    """

    host: pydantic.HttpUrl = pydantic.HttpUrl(DID_RESOLVER_HOST or "http://localhost:8080/")
