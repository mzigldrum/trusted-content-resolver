Feature: Testing Trusted Content Resolver REST API

  Scenario: Run first simple test
     Given we have XFSC TRAIN Trusted Content Resolver REST API
       And an actor X to be verified
      When we initiate a verification actor X
      Then actor X gets verified

#  Scenario: Run first simple test
#    Given we have XFSC TRAIN Trusted Content Resolver REST API
#    And an actor X to be verified
#    When we initiate a verification actor Y
#    Then actor X gets verified
#
#  Scenario: [IDM.TRAIN.00014] Trust Framework Configuration success
#    Given A request update of trust frameworks and DID configuration is successfully reflected in the DNS Zone File (200)
#    And An instantiation of a trust list is reflected in the trust list storage with possibility to retrieve via API endpoints
#    When Creation of a VC is allowed with ability to sign the credential
#    Then An error is provided if a record is in progress by the operator
#     And Should be able to reference Trust Frameworks from other Domains
#
#  Scenario: [IDM.TRAIN.00014] Trust Framework Configuration 400
#    Given A request update of trust frameworks and DID configuration is successfully reflected in the DNS Zone File (200)
#    And An instantiation of a trust list is reflected in the trust list storage with possibility to retrieve via API endpoints
#    When A wrong context or missing data leads to an exception (400)
#    And An audit entry is created
#    Then An error is provided if a record is in progress by the operator
#    And Should be able to reference Trust Frameworks from other Domains
#
#
#  Acceptance Criteria
#  The following acceptance criteria MUST be met:
#  1. Use standardized DNS resolvers
#  2. Use standardized DID resolver
#
#
#  Given trust framework pointers example.federation1.de and example.federation2.de
#  # 2. Issuer details from the VC/VP (e.g., DID/URI)
#  #3. ServiceType of the trust list (e.g., issuance service, verifier service)
#   When Navigate to listed trust framework pointers
#   Then have example.federation1.de and example.federation2.de should be in trust List VC endpoint
#    And have Corresponding DID mapped to example.federation1.de and example.federation2.de
#     And exists DID Document of the DID
