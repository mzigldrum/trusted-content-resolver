#!/usr/bin/env bash

set -eu -o pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR/../../../../clients/java

## Collect all jars dependencies into single folder
test !-d all_dependent_jars && mvn dependency:copy-dependencies -DoutputDirectory=all_dependent_jars -q

## Collect jars into CLASSPATH
CLASSPATH_="$(pwd)/target/trusted-content-resolver-java-client-1.0.0-SNAPSHOT.jar"
for i in $(pwd)/all_dependent_jars/*.jar; do CLASSPATH_=$CLASSPATH_:$i; done

## Execute example script
java -cp $CLASSPATH_ $SCRIPT_DIR/TrustContentResolverExample.java
