package eu.xfsc.train.tcr.server.legacy;

import org.springframework.beans.factory.annotation.Value;
//import eu.lightest.verifier.ATVConfiguration;
//import eu.lightest.verifier.exceptions.DNSException;

import org.jitsi.dnssec.validator.ValidatingResolver;
import org.xbill.DNS.*;
import org.xbill.DNS.Record;

import eu.xfsc.train.tcr.server.exception.DnsException;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.List;
import java.util.stream.Stream;

@Slf4j
public class DNSHelper {
    
    public static final String DNS_GOOGLE1 = "8.8.8.8"; // https://developers.google.com/speed/public-dns/
    public static final String DNS_GOOGLE2 = "8.8.4.4"; // https://developers.google.com/speed/public-dns/
    public static final String DNS_CLOUDFLARE1 = "1.1.1.1"; // https://1.1.1.1/
    public static final String DNS_ESSIF = "137.251.108.68";
    public static final String DNS_CLOUDFLARE2 = "1.0.0.1"; // https://1.0.0.1/
    public static final String DNS_CLOUDFLARE1v6 = "[2606:4700:4700::1111]";
    public static final String DNS_CLOUDFLARE2v6 = "[2606:4700:4700::1001]";
    public static final String DNS_CISCOopendns1 = "208.67.222.222";
    public static final String DNS_CISCOopendns2 = "208.67.220.220";
    public static final String DNS_CISCO = "171.70.168.183";
    public static final String DNS_EDIS = "151.236.4.166";
    public static final String CNS_CCC = "213.73.91.35";
    public static final int RECORD_A = Type.A;
    public static final int RECORD_CNAME = Type.CNAME;
    public static final int RECORD_URI = Type.URI;
    public static final int RECORD_PTR = Type.PTR;
    public static final int RECORD_TXT = Type.TXT;
    public static final int RECORD_TLSA = Type.TLSA;
    public static final int RECORD_SMIMEA = Type.SMIMEA;

    private final static String ROOT_PATH = "ROOT_KEY"; //ATVConfiguration.get().getString("dnssec_root_key");
    @Value("${tcr.dns.dnssecVerificationEnabled}")
    private boolean dnssecVerificationEnabled;
    
    //private final static String ROOT_PATH2 = "/var/lib/unbound/root.key";
    
    private final static String DNSROOT = ". IN DNSKEY 257 3 8 AwEAAaz/tAm8yTn4Mfeh5eyI96WSVexTBAvkMgJzkKTOiW1vkIbzxeF3+/4RgWOq7HrxRixHlFlExOLAJr5emLvN7SWXgnLh4+B5xQlNVz8Og8kvArMtNROxVQuCaSnIDdD5LKyWbRd2n9WGe2R8PzgCmr3EgVLrjyBxWezF0jLHwVN8efS3rCj/EWgvIWgb9tarpVUDK/b58Da+sqqls3eNbuv7pr+eoZG+SrDK6nWeL3c6H5Apxz7LjVc1uTIdsIXxuOLYA4/ilBmSVIzuDWfdRUfhHdY6+cn8HFRm+2hM8AnXGXws9555KrUB5qihylGa8subX2Nn6UwNR1AkUTV74bU=";
    
    private final Resolver resolver;
    
    public DNSHelper() { //throws IOException {
        //this(DNS_CLOUDFLARE1);
        this(DNS_GOOGLE1); //ESSIF);
    }
    
    public DNSHelper(String dnsServerHostname) { //throws IOException {
        this(dnsServerHostname, ROOT_PATH);
    }
    
    private DNSHelper(String dnsServerHostname, String rootPath) { //throws IOException {
        File f = new File(rootPath);
        InputStream rootInputStream = null;
        
        try {
            if (f.exists()) {
                rootInputStream = new FileInputStream(rootPath);
            } else {
                InputStream inputStream = getClass().getClassLoader().getResourceAsStream(rootPath);
                if (inputStream != null) {
                    rootInputStream = inputStream;
                } else {
                    log.error("DNSSEC Root-key: File Not Found: " + f.getAbsolutePath() + " (configured: " + rootPath + "). Using hardcoded backup.");
                    rootInputStream = new ByteArrayInputStream(DNSROOT.getBytes());
                }
            }
            SimpleResolver simpleResolver = new SimpleResolver(); //dnsServerHostname);
            
            if (dnssecVerificationEnabled) {
                ValidatingResolver validatingResolver = new ValidatingResolver(simpleResolver);
                validatingResolver.loadTrustAnchors(rootInputStream);
                this.resolver = validatingResolver;
            } else {
                this.resolver = simpleResolver;
                
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (rootInputStream != null) {
                try {
					rootInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
            }
        }
    }
    
    
    public Stream<Record> parseMessage(Message response) throws IOException, DnsException {
        
        List<RRset> rrSets = response.getSectionRRsets(Section.ANSWER);
        return rrSets.stream().flatMap(s -> s.rrs().stream());
    }
    
    public List<String> queryTXT(String host) throws IOException, DnsException {

    	Message response = query(host, Type.TXT);
        Stream<Record> records = parseMessage(response);
        return records.filter(r -> r instanceof TXTRecord)
            .flatMap(r -> ((TXTRecord) r).getStrings().stream())
            .toList();
    }
    
    public List<String> queryPTR(String host) throws IOException, DnsException {

    	Message response = query(host, Type.PTR);
        Stream<Record> records = parseMessage(response);
        return records.filter(r -> r instanceof PTRRecord)
            .map(r -> ((PTRRecord) r).getTarget().toString())
            .toList();
    }
    
    public List<String> queryURI(String host) throws IOException, DnsException {
        
    	Message response = query(host, Type.URI);
        Stream<Record> records = parseMessage(response);
        return records.filter(r -> r instanceof URIRecord)
        	.map(r -> ((URIRecord) r).getTarget())
        	.toList();
    }
    
    public List<SMIMEAcert> querySMIMEA(String host) throws IOException, DnsException {
        // https://tools.ietf.org/html/rfc6698#section-2
        
        Message response = query(host, Type.SMIMEA);
        Stream<Record> records = parseMessage(response);
        return records.filter(r -> r instanceof SMIMEARecord)
       		.map(r -> new SMIMEAcert((SMIMEARecord) r))
       		.toList();
    }
    
    
    public Message query(String host, int type) throws IOException, DnsException {
        if (!host.endsWith(".")) {
            host = host + ".";
        }
        
        Record query = Record.newRecord(Name.fromConstantString(host), type, DClass.IN);
        log.debug("query; DNS query: " + query.toString());
        Message response = resolver.send(Message.newQuery(query));
        if (!response.getHeader().getFlag(Flags.AD)) {
            log.debug("query; NO AD flag present!");
            //throw new DNSException("No AD flag. (Host not using DNSSec?)");
        }
        
        int rcode = response.getRcode();
        if (rcode != Rcode.SERVFAIL) {
            if (rcode != Rcode.NOERROR) {
                for (RRset set: response.getSectionRRsets(Section.ADDITIONAL)) {
                    log.debug("query; Zone: {}", set.getName());
                    if (set.getName().equals(Name.root) && set.getType() == Type.TXT && 
                    		set.getDClass() == ValidatingResolver.VALIDATION_REASON_QCLASS) {
                        log.debug("query; Reason: {}", ((TXTRecord) set.first()).getStrings().get(0));
                    }
                }
                throw new DnsException("RCode: " + rcode + " (" + Rcode.string(rcode) + ")");
            }
        }
        return response;
    }
    
    public <R extends Record> List<R> queryAndParse(String host, Class recordTypeClass, int recordTypeID) throws IOException, DnsException {
        
        Message response = query(host, recordTypeID);
        List<RRset> rrSets = response.getSectionRRsets(Section.ANSWER);
        return rrSets.stream().flatMap(s -> s.rrs().stream()).map(r -> (R) r).toList();
    }
    
    
}
