package eu.xfsc.train.tcr.server.legacy;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.xfsc.train.tcr.server.exception.DnsException;

@Slf4j
public class TrustSchemeFactory {
	
	@Value("${tcr.dns.verification.enabled}")
	private boolean daneVerificationEnabled;
    
    public static final String CLAIM_PREFIX = "_scheme._trust";
    private DNSHelper dns = new DNSHelper();
    private HTTPSHelper https = new HTTPSHelper();
    private SMIMEAHelper smimea = new SMIMEAHelper(dns);
    
    
    public Collection<TrustScheme> createTrustSchemes(TrustSchemeClaim claim) throws IOException, DnsException {

    	Collection<TrustScheme> schemes = new ArrayList<>();
        List<String> schemeHostNames = discoverTrustSchemes(claim);
    
        for (String schemeHostName: schemeHostNames) {
            List<String> tslLocations = discoverTrustLists(schemeHostName);
            for (String tslLocation: tslLocations) {
            	String tslContent = loadTrustList(tslLocation);
            	if (tslContent != null) {
            		boolean isValid = true;
            		if (daneVerificationEnabled) {
            			isValid = verifyTrustList(schemeHostName, tslContent);
            		}
            		if (isValid) {
            	        schemes.add(new TrustScheme(tslLocation, schemeHostName, tslContent));
            		}
            	}
            }
        }
        return schemes;
    }
    
    private boolean verifyTrustList(String schemeHostname, String tslContent) throws IOException {
        return smimea.verifyXMLdocument(schemeHostname, tslContent);
    }
    
    private String loadTrustList(String tslLocation) {
        log.debug("loadTrustList.enter; got location: {}", tslLocation);
        String trustList = null;
        try {
            trustList = https.get(new URL(tslLocation));
        } catch (IOException e) {
            log.warn("loadTrustList.error", e);
        }
        log.debug("loadTrustList.exit; returning: {}", trustList);
        return trustList;
    }
    
    private List<String> discoverTrustSchemes(TrustSchemeClaim claim) {
        log.debug("discoverTrustSchemes.enter; got claim: {}", claim);
        String hostname = buildHostname(claim);
        
        List<String> schemes;
        try {
            schemes = dns.queryPTR(hostname);
        } catch (IOException | DnsException e) {
            log.warn("discoverTrustScheme.error", e);
            schemes = Collections.emptyList();
        }
        log.debug("discoverTrustSchemes.exit; returning schemes: {}", schemes);
        return schemes;
    }
    
    private String buildHostname(TrustSchemeClaim claim) {
        String claimhost = claim.getClaim();
        if (claimhost.startsWith(CLAIM_PREFIX)) {
            return claimhost;
        } else {
            return TrustSchemeFactory.CLAIM_PREFIX + "." + claimhost;
        }
    }
    
    private List<String> discoverTrustLists(String schemeHostName) {
        log.debug("discoverTrustLists.enter; got scheme: {}", schemeHostName);
        List<String> lists;
        try {
            lists = dns.queryURI(schemeHostName);
        } catch (IOException | DnsException e) {
            log.warn("discoverTrustLists.error", e);
            lists = Collections.emptyList();
        }
        log.debug("discoverTrustLists.exit; returning lists: {}", lists);
        return lists;
    }
}
