Feature:  validate the output of the trust discovery functionality of the Trusted Content Resolver
  validate the association of DID with a well-known DID configuration
  validate the integrity of the VC
  validate the issuer details from the trust lists extracted from service endpoints
  integrate by TRAIN client libraries (go, java, js, py)

Background: fully environment setup

  Given that the TCR is running
    And The DID Resolver is running
    And The DNS entry is configured
    And DNSSEC is correct
    And Multiple Trust Framework Pointers exist (example.federation1.de, example.federation2.de)
    And client `Trust-content-resolver-client-validator-java` installed
    And client `Trust-content-resolver-client-validator-py` installed
    And client `Trust-content-resolver-client-validator-go` installed
    And client `Trust-content-resolver-client-validator-js` installed

# make 2 use case:
# 1. RSA
# 2. ECDSA
# (1) and (2) combination with (VALID, INVALID, INDETERMINATE)
Scenario: 00017-A1_VC validation mechanism supports multiple signature proofs (RSA, ECDSA)

  Given the fully environment setup
    And Corresponding DID mapped to Trust Framework Pointer 
    And DID Document of the DID registered
    And Trust List VC endpoint available
   When the Trusted Content Resolver (TCR) reads the PTR RRs of the DNS Domain resolved from the Trust Framework Pointer
   Then the 'Well Known DID configuration' verification is performed for DID-method "web"
    And the DID Document is resolved which leads to a VC via its Service Endpoint
    And the proof of the VC is validated against the public keys of the DID Document using cryptograhic libraries
    And the result of the VC is validated (result: VALID, INVALID, INDETERMINATE)
    And multiple signature proofs (RSA, ECDSA) are supported
    And the Credential Subject of the VC is ready to obtain the URI of the Trust List (at a https URL)
    And the trust list is resolved
    And the TCR checks that the specific entity is listed in the trust list
    And the TCR will return that the claimed entity is a member of the trust framework operated by "DNS name"
    And the VC schema can be checked
