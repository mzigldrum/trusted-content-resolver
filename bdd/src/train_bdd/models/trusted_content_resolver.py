"""
Trusted Content Resolver Model
"""
import pydantic
import requests
import requests.exceptions

from .._env import HOST
from ._spring_boot_actuator import SpringBootActuator
from .trust_framework_pointer import TrustFrameworkPointer


class TrustedContentResolver(SpringBootActuator):
    """
    Trusted Content Resolver / Extended Universal Resolver (TCR for short) + Libraries

    See `https://gitlab.eclipse.org/eclipse/xfsc/train/trusted-content-resolver/-/blob/main/openapi/tcr_openapi.yaml`_
    """

    host: pydantic.HttpUrl = pydantic.HttpUrl(HOST or "http://localhost:8087")

    def resolve(self,
                did: str,
                trust_framework_pointers: set[TrustFrameworkPointer]) -> tuple[dict[str, str], ...]:
        """
        Invoke `resolve` REST endpoint
        """
        data = []
        for tfp in trust_framework_pointers:
            response = requests.post(
                url=f"{self.host}resolve",
                json={
                    'issuer': did,
                    'trustSchemePointer': tfp
                },
                headers={
                    'Content-Type': "application/json",
                },
                timeout=1,
            )
            assert response.status_code == 200
            data.append(response.json())

        return tuple(data)
