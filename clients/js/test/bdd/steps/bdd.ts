import { binding, given, then, when} from 'cucumber-tsflow';
import { assert } from 'chai';

@binding()
export class TrustedContentResolver {
    private app: string = "";
    private actor_x: Object = {}
    private response: string = "";

    @given(/we have XFSC TRAIN Trusted Content Resolver REST API/)
    public stepImpl1() {
        this.app = 'http://localhost:8087';
    }
    @given(/an actor X to be verified/)
    public stepImpl2() {
        // TODO check the data if available
        // load input data for actor x into context
        this.actor_x = {
            'some': 'info'
        }
    }

    @when(/we initiate a verification actor X/)
    public stepImpl3() {
        this.response = 'SESSION.get(f\'https://example.com\')'
    }

    @then(/actor X gets verified/)
    public stepImpl4() {
        assert.equal(this.response, 'SESSION.get(f\'https://example.com\')');
    }

    @then(/we initiate a verification actor Y/)
    public stepImpl5() {
        // TODO: implement
    }
}
