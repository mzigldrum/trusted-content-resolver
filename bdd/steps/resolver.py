from typing import Any, Union
import json

import bash
from behave import given, when, then
from behave.model import Text
from behave.runner import Context

from train_bdd.models.trust_list import TrustList
from train_bdd.models.trusted_content_resolver import TrustedContentResolver
from train_bdd.models.universal_decentralized_identifiers_resolver import UniversalDecentralizedIdentifiersResolver
from train_bdd.models.domain_name_system_resolver import DomainNameSystemResolver
from train_bdd.models.trust_framework_pointer import TrustFrameworkPointer
from train_bdd.models.client.py import Py
from train_bdd.models.client.java import Java

TCR = TrustedContentResolver()
DID_RESOLVER = UniversalDecentralizedIdentifiersResolver()


class ContextType(Context):
    dns_resolver: DomainNameSystemResolver
    trust_framework_pointers: set[TrustFrameworkPointer]
    resolve_response: dict[str, Any]
    text: Text
    trust_list: TrustList
    client: Union[Py, Java]


@given("TCR is running")
def step_impl(context: ContextType):
    assert TCR.is_up(), "is not up"


@given("DID Resolver is running")
def step_impl(context: ContextType):
    assert DID_RESOLVER.is_up(), "is not up"


@given("{domain_mame_system_type} with {ip} is running")
def step_impl(context: ContextType, domain_mame_system_type: str, ip: str):
    context.dns_resolver = DomainNameSystemResolver.init(
        implementation=domain_mame_system_type,
        ip=ip
    )
    assert context.dns_resolver.is_up(), "is not up"


@given("trust framework pointers are `{configured_or_misconfigured}` in DNS")
def step_impl(context: ContextType, configured_or_misconfigured: str):
    assert context.trust_framework_pointers
    for tfp in context.trust_framework_pointers:
        if configured_or_misconfigured == "configured":
            assert context.dns_resolver.configured_resolve_trust_framework_pointer(tfp), \
                "trust framework pointers not configured"
        elif configured_or_misconfigured == "misconfigured":
            assert not context.dns_resolver.configured_resolve_trust_framework_pointer(tfp), \
                "trust framework pointers configured"
        else:
            raise NotImplementedError(configured_or_misconfigured)


@given("client {client_name} installed")
def step_impl(context: ContextType, client_name: str):
    if client_name == 'Trust-content-resolver-client-validator-java':
        context.client = Java()
    elif client_name == 'Trust-content-resolver-client-validator-py':
        context.client = Py()
    elif client_name == 'Trust-content-resolver-client-validator-go':
        print('mock all ok')
        return
    elif client_name == 'Trust-content-resolver-client-validator-js':
        print('mock all ok')
        return
    else:
        raise NotImplementedError(client_name)

    assert context.client.is_up()


@given("multiple Trust Framework Pointers")
def step_impl(context: ContextType):
    context.trust_framework_pointers = TrustFrameworkPointer.from_text(context.text)
    assert context.trust_framework_pointers, "no Trust Framework Pointers available"


@given("Validate DNS Name against DNSSEC")
def step_impl(context: ContextType):
    return context.dns_resolver.security_extensions


@when("above Trust Framework Pointers are supplied in resolver trust request by `{did}`")
def step_impl(context: ContextType, did):
    if hasattr(context, 'client'):
        context.resolve_response = context.client.resolve(
            trust_framework_pointers=context.trust_framework_pointers, did=did
        )
    else:
        context.resolve_response = TCR.resolve(
            trust_framework_pointers=context.trust_framework_pointers, did=did
        )
    print("Got resolve response as:", json.dumps(context.resolve_response))


@then("Trust List's corresponding Trust Framework pointers from context")
def step_impl(context: ContextType):
    tfp_from_tl = TrustList.fetch().trust_framework_pointers
    assert tfp_from_tl.intersection(context.trust_framework_pointers), \
        f"{tfp_from_tl=} does no intersect {context.trust_framework_pointers=}"


@then("Trust List's will be emtpy")
def step_impl(context: ContextType):
    assert len(TrustList.fetch()) == 0
