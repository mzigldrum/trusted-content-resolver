package eu.xfsc.train.tcr.server.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.net.URL;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import eu.europa.esig.trustedlist.jaxb.tsl.TrustStatusListType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class TLResolverTest {
	
	private static final String fh_TrustList_Uri = "https://tspa.trust-scheme.de/tspa_train_domain/api/v1/scheme/federation1.test.train.trust-scheme.de";
	private static final String fh_TrustList_Hash = "Qmb6gRAKQmVopCVDs1TMspu3crtoQ7JLLWY6rYNb5yVeWY"; 

	@Autowired
	private TLResolver tlResolver;

	@Test
	public void testTLResolveXMLFile() throws Exception {
		URL url = this.getClass().getResource("/SampleTrustList.xml");
		File file = new File(url.getFile());
		assertTrue(file.exists());
		TrustStatusListType list = tlResolver.resolveTL(file.getCanonicalFile().toURI().toString());
		assertNotNull(list);
	}

	@Test
	public void testTLResolveJSONFile() throws Exception {
		URL url = this.getClass().getResource("/SampleTrustList.json");
		File file = new File(url.getFile());
		assertTrue(file.exists());
		TrustStatusListType list = tlResolver.resolveTL(file.getCanonicalFile().toURI().toString());
		assertNotNull(list);
	}

	@Test
	public void testTLResolveXMLUri() throws Exception {
		TrustStatusListType list = tlResolver.resolveTL(fh_TrustList_Uri, false);
		assertNotNull(list);
	}

	@Test
	public void testTLResolveWithHash() throws Exception {
		TLResolver.TLResolveResult tlRes = tlResolver.resolveTLHash(fh_TrustList_Uri, fh_TrustList_Hash);
		assertNotNull(tlRes);
		assertTrue(tlRes.isHashVerified());
	}

	@Test
	public void testTLValidationXML() throws Exception {
		URL url = this.getClass().getResource("/schuelerausweis.xml");
		File file = new File(url.getFile());
		assertTrue(file.exists());

		boolean valid = tlResolver.validateTL(file.getCanonicalFile().toURI().toString());
		assertTrue(valid);
	}

	@Test
	public void testTLValidationJSON() throws Exception {
		URL url = this.getClass().getResource("/FHTrustList.json");
		File file = new File(url.getFile());
		assertTrue(file.exists());
		boolean valid = tlResolver.validateTL(file.getCanonicalFile().toURI().toString());
		assertTrue(valid);
	}
	
	@Test
	public void testValidateSampleTrustList() throws Exception {
		// File cache = new File("cache");
		// cache.deleteOnExit();

		URL url = this.getClass().getResource("/SampleTrustList.xml");
		File file = new File(url.getFile());
		assertTrue(file.exists());

		tlResolver.validateDocument(file.getPath());
		// assertTrue(valid);
	}

	@Test
	public void testValidateTL_21() throws Exception {
		URL url = this.getClass().getResource("/TL-21.xml");
		File file = new File(url.getFile());
		assertTrue(file.exists());

		long stamp = System.currentTimeMillis();
		tlResolver.validateDocument(file.getPath());
		stamp = System.currentTimeMillis() - stamp;
		log.info("testValidateTL_21; first validation took {} ms", stamp);
		
		stamp = System.currentTimeMillis();
		tlResolver.validateDocument(file.getPath());
		stamp = System.currentTimeMillis() - stamp;
		log.info("testValidateTL_21; second validation took {} ms", stamp);
	}
	
}