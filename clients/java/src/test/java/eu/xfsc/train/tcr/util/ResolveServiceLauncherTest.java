package eu.xfsc.train.tcr.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import eu.xfsc.train.tcr.api.generated.model.ResolveResult;
import eu.xfsc.train.tcr.api.generated.model.ResolveTrustList;
import eu.xfsc.train.tcr.api.generated.model.ValidateResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ResolveServiceLauncherTest {
	
    private static MockWebServer mockBackEnd;
    private static final int TCR_PORT = 8087;
    private static final ObjectMapper mapper = new ObjectMapper(); 
	private static final TypeReference<List<ResolveResult>> LIST_TYPE_REF = new TypeReference<List<ResolveResult>>() {};
    
    @BeforeAll
    static void setUp() throws IOException {
        mockBackEnd = new MockWebServer();
        mockBackEnd.noClientAuth();
        mockBackEnd.start(TCR_PORT);
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockBackEnd.shutdown();
    }
    
	@Test
	public void testLauncherFailEmptyRequest() throws Exception {
		String[] arguments = new String[] {};

	    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	    PrintStream out = new PrintStream(byteArrayOutputStream);
	    System.setOut(out);
		
    	ResolveServiceLauncher.main(arguments);

	    String output = byteArrayOutputStream.toString(Charset.defaultCharset());
	    assertEquals("error: no resolution endpoint specified\n", output);
	    out.close();
	}

	@Test
	public void testLauncherFailOptionsRequest() throws Exception {
		String[] arguments = new String[] {"-Dtrain.opt=value", "--flag-on", "-d", "endpoint=validate"};

	    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	    PrintStream out = new PrintStream(byteArrayOutputStream);
	    System.setOut(out);
		
    	ResolveServiceLauncher.main(arguments);

	    String output = byteArrayOutputStream.toString(Charset.defaultCharset());
	    assertTrue(output.endsWith("error: no validation data provided\n"));
	    out.close();
	}
	
	@Test
	public void testLauncherFailNoEndpointRequest() throws Exception {
		String[] arguments = new String[] {"d={\"issuer\": \"issuer1\", \"trustSchemePointers\": [\"ptr1\", \"ptr2\"]}"};

	    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	    PrintStream out = new PrintStream(byteArrayOutputStream);
	    System.setOut(out);
		
    	ResolveServiceLauncher.main(arguments);

	    String output = byteArrayOutputStream.toString(Charset.defaultCharset());
	    assertEquals("error: no resolution endpoint specified\n", output);
	    out.close();
	}

	@Test
	public void testLauncherFailUnknownEndpointRequest() throws Exception {
		String[] arguments = new String[] {"e=verify"};

	    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	    PrintStream out = new PrintStream(byteArrayOutputStream);
	    System.setOut(out);

	    ResolveServiceLauncher.main(arguments);

	    String output = byteArrayOutputStream.toString(Charset.defaultCharset());
	    assertEquals("error: unknown resolution endpoint: verify\n", output);
	    out.close();
	}

	@Test
	public void testLauncherFailNoDataRequest() throws Exception {
		String[] arguments = new String[] {"e=validate"};

	    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	    PrintStream out = new PrintStream(byteArrayOutputStream);
	    System.setOut(out);

	    ResolveServiceLauncher.main(arguments);

	    String output = byteArrayOutputStream.toString(Charset.defaultCharset());
	    assertEquals("error: no validation data provided\n", output);
	    out.close();
	}

	@Test
	public void testLauncherResolveResponse() throws Exception {
		String[] arguments = new String[] {"e=resolve", "d={\"issuer\": \"issuer1\", \"trustSchemePointers\": [\"ptr1\", \"ptr2\"]}"};

	    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	    PrintStream out = new PrintStream(byteArrayOutputStream);
	    System.setOut(out);
		
		ResolveResult rrs = new ResolveResult("did:mock:sample-issuer1", List.of("ptr2"), Map.of(), List.of(new ResolveTrustList("https://issuer1.test/trust-list", Map.of())));
		mockBackEnd.enqueue(new MockResponse().setBody(mapper.writeValueAsString(List.of(rrs))).addHeader("Content-Type", "application/json"));

    	ResolveServiceLauncher.main(arguments);

	    String output = byteArrayOutputStream.toString(Charset.defaultCharset());
	    assertTrue(output.startsWith("result: "));
	    List<ResolveResult> resp = mapper.readValue(output.substring(8), LIST_TYPE_REF);
	    assertEquals(1, resp.size());
	    assertEquals(rrs.getDid(), resp.get(0).getDid());
	    assertEquals(1, rrs.getPointers().size());
	    assertEquals("ptr2", rrs.getPointers().get(0));

	    out.close();
	}

	@Test
	public void testLauncherValidateResponse() throws Exception {
		String[] arguments = new String[] {"e=validate", "d={\"issuer\": \"issuer1\", \"did\": \"did:mock:sample-issuer1\", \"endpoints\": [\"https://issuer1.test/trust-list\"]}"};

	    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	    PrintStream out = new PrintStream(byteArrayOutputStream);
	    System.setOut(out);
		
		ValidateResponse vrs = new ValidateResponse(true, true, true, List.of(new ResolveTrustList("https://issuer1.test/trust-list", Map.of())));
		mockBackEnd.enqueue(new MockResponse().setBody(mapper.writeValueAsString(vrs)).addHeader("Content-Type", "application/json"));

    	ResolveServiceLauncher.main(arguments);

	    String output = byteArrayOutputStream.toString(Charset.defaultCharset());
	    assertTrue(output.startsWith("result: "));
	    ValidateResponse resp = mapper.readValue(output.substring(8), ValidateResponse.class);
	    assertEquals(vrs, resp);

	    out.close();
	}

	@Test
	public void testLauncherResolveFileResponse() throws Exception {
		String[] arguments = new String[] {"uri=http://localhost:8087", "f=./src/test/resources/resolve-input.json"};

	    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	    PrintStream out = new PrintStream(byteArrayOutputStream);
	    System.setOut(out);
		
		ResolveResult rrs = new ResolveResult("did:mock:sample-issuer1", List.of("ptr2"), Map.of(), List.of(new ResolveTrustList("https://issuer1.test/trust-list", Map.of())));
		mockBackEnd.enqueue(new MockResponse().setBody(mapper.writeValueAsString(List.of(rrs))).addHeader("Content-Type", "application/json"));

    	ResolveServiceLauncher.main(arguments);

	    String output = byteArrayOutputStream.toString(Charset.defaultCharset());
	    assertTrue(output.startsWith("result: "));
	    List<ResolveResult> resp = mapper.readValue(output.substring(8), LIST_TYPE_REF);
	    assertEquals(1, resp.size());
	    assertEquals(rrs.getDid(), resp.get(0).getDid());
	    assertEquals(1, rrs.getPointers().size());
	    assertEquals("ptr2", rrs.getPointers().get(0));

	    out.close();
	}

	@Test
	public void testLauncherValidateFileResponse() throws Exception {
		String[] arguments = new String[] {"f=src/test/resources/validate-input.json"};
		
        mockBackEnd.shutdown();
        mockBackEnd = new MockWebServer();
        mockBackEnd.noClientAuth();
        mockBackEnd.start(8080); 

	    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	    PrintStream out = new PrintStream(byteArrayOutputStream);
	    System.setOut(out);
		
		ValidateResponse vrs = new ValidateResponse(true, true, true, List.of(new ResolveTrustList("https://issuer1.test/trust-list", Map.of())));
		mockBackEnd.enqueue(new MockResponse().setBody(mapper.writeValueAsString(vrs)).addHeader("Content-Type", "application/json"));

    	ResolveServiceLauncher.main(arguments);

	    String output = byteArrayOutputStream.toString(Charset.defaultCharset());
	    assertTrue(output.startsWith("result: "));
	    ValidateResponse resp = mapper.readValue(output.substring(8), ValidateResponse.class);
	    assertEquals(vrs, resp);

	    out.close();
	    
	    tearDown();
	    setUp();
	}
	
}
