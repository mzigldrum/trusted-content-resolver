#!/bin/env bash

export TRAIN_TRUST_CONTENT_RESOLVER_HOST="http://localhost:8887"
source TRAIN_TRUST_CONTENT_RESOLVER_CLIENT_PY_VENV="/opt/python.d/dev/train/trusted_content_resolver_client"
source TRAIN_TRUST_CONTENT_RESOLVER_CLIENT_JAVA_TARGET="/Users/app/trusted-content-resolver/clients/java/target/"
