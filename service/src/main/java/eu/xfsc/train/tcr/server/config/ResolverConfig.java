package eu.xfsc.train.tcr.server.config;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.List;

import org.jitsi.dnssec.validator.ValidatingResolver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xbill.DNS.ExtendedResolver;
import org.xbill.DNS.SimpleResolver;

import com.apicatalog.jsonld.loader.DocumentLoader;

import org.xbill.DNS.Resolver;

import eu.xfsc.train.tcr.server.exception.DidException;
import eu.xfsc.train.tcr.server.exception.DnsException;
import foundation.identity.jsonld.ConfigurableDocumentLoader;
import lombok.extern.slf4j.Slf4j;
import uniresolver.UniResolver;
import uniresolver.client.ClientUniResolver;

@Slf4j
@Configuration
public class ResolverConfig {

    private final static String DNSROOT = ". IN DNSKEY 257 3 8 AwEAAaz/tAm8yTn4Mfeh5eyI96WSVexTBAvkMgJzkKTOiW1vkIbzxeF3+/4RgWOq7HrxRixHlFlExOLAJr5emLvN7SWXgnLh4+B5xQlNVz8Og8kvArMtNROxVQuCaSnIDdD5LKyWbRd2n9WGe2R8PzgCmr3EgVLrjyBxWezF0jLHwVN8efS3rCj/EWgvIWgb9tarpVUDK/b58Da+sqqls3eNbuv7pr+eoZG+SrDK6nWeL3c6H5Apxz7LjVc1uTIdsIXxuOLYA4/ilBmSVIzuDWfdRUfhHdY6+cn8HFRm+2hM8AnXGXws9555KrUB5qihylGa8subX2Nn6UwNR1AkUTV74bU=";
	
	@Bean
	public UniResolver uniResolver(@Value("${tcr.did.base-uri}") String baseUri,
			@Value("${tcr.did.timeout}") int timeout) {
		log.info("uniResolver.enter; configured base URI: {}, timeout: {}", baseUri, timeout);
		UniResolver resolver;
		URI uri;
		try {
			uri = new URI(baseUri);
		} catch (URISyntaxException ex) {
			log.error("uniResolver.error", ex);
			throw new DidException(ex);
		}
		resolver = ClientUniResolver.create(uri);
		log.info("uniResolver.exit; returning resolver: {}", resolver);
		return resolver;
	}

	@Bean
	public Resolver resolver(@Value("${tcr.dns.hosts}") List<String> dnsHosts,
			@Value("${tcr.dns.timeout}") int timeout,
		    @Value("${tcr.dns.dnssec.rootPath}") String dnssecRootPath, 
		    @Value("${tcr.dns.dnssec.enabled}") boolean dnssecEnabled) {
		log.info("resolver.enter; configured DNS hosts: {}, timeout: {}, dnssecRootPath: {}, dnssecEnaabled: {}", dnsHosts, timeout, dnssecRootPath, dnssecEnabled);
		Resolver resolver;
		try {
			if (dnsHosts.isEmpty()) {
				resolver = new SimpleResolver();
			} else {
				SimpleResolver[] resolvers = new SimpleResolver[dnsHosts.size()];
				int idx = 0;
				for (String dnsHost : dnsHosts) {
					SimpleResolver r = new SimpleResolver(dnsHost);
					if (timeout > 0) {
						r.setTimeout(Duration.ofMillis(timeout));
					}
					resolvers[idx++] = r;
				}
				resolver = new ExtendedResolver(resolvers);
			}
			if (timeout > 0) {
				resolver.setTimeout(Duration.ofMillis(timeout));
			}
			
			if (dnssecEnabled) {
				InputStream rootInputStream;
				File f = new File(dnssecRootPath);
				if (f.exists()) {
			        rootInputStream = new FileInputStream(dnssecRootPath);
			    } else {
			        rootInputStream = getClass().getClassLoader().getResourceAsStream(dnssecRootPath);
			        if (rootInputStream == null) {
			            log.info("resolve; DNSSEC Root-key not found, using hardcoded backup.");
			            rootInputStream = new ByteArrayInputStream(DNSROOT.getBytes());
			        }
			    }

		        ValidatingResolver validatingResolver = new ValidatingResolver(resolver);
		        validatingResolver.loadTrustAnchors(rootInputStream);
		        resolver = validatingResolver;
			}
		} catch (IOException ex) {
			log.error("resolver.error", ex);
			throw new DnsException(ex);
		}
		log.info("resolver.exit; returning resolver: {}", resolver);
		return resolver;
	}

	@Bean
	public DocumentLoader documentLoader() {
		ConfigurableDocumentLoader loader = new ConfigurableDocumentLoader();
		// TODO: get settings from app props
		loader.setEnableHttp(true);
		loader.setEnableHttps(true);
		return loader;
	}
}

