package eu.xfsc.train.tcr.server.service;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jitsi.dnssec.validator.ValidatingResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xbill.DNS.DClass;
import org.xbill.DNS.Flags;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.PTRRecord;
import org.xbill.DNS.RRset;
import org.xbill.DNS.Rcode;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.Section;
import org.xbill.DNS.TXTRecord;
import org.xbill.DNS.Type;
import org.xbill.DNS.URIRecord;

import eu.xfsc.train.tcr.server.exception.DnsException;
import eu.xfsc.train.tcr.server.legacy.TrustScheme;
import eu.xfsc.train.tcr.server.legacy.TrustSchemeClaim;
import eu.xfsc.train.tcr.server.legacy.TrustSchemeFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DNSResolver {
	
	private static final String PREFIX = "_scheme._trust.";
	
	@Autowired
	private Resolver resolver;
	
	public Collection<String> resolveDomain(String domain) throws Exception {
		Set<String> results = new HashSet<>();
	    Set<String> uris = resolvePtr(domain); // we can get another PTRs here?
	    for (String uri: uris) {
	        Set<String> dids = resolveUri(uri);
	        results.addAll(dids);
	    }
		return results;
	}
	
	public Collection<String> resolveDomains(List<String> domains) throws Exception {
		Set<String> results = new HashSet<>();
		for (String domain: domains) {
	        Set<String> uris = resolvePtr(domain); // we can get another PTRs here?
	        for (String uri: uris) {
	            Set<String> dids = resolveUri(uri);
	            results.addAll(dids);
	        }
	    }
		return results;
	}
	
    /**
     * resolves DNS PTR record
     * 
     * @param ptr - the PTR record to query
     * @return the resolved Set<String> 
     */
    private Set<String> resolvePtr(String ptr) {
        log.debug("resolvePtr.enter; got PTR: {}", ptr);
        Set<String> set = query(ptr, Type.PTR)
           	.filter(r -> r instanceof PTRRecord)
            .map(r -> ((PTRRecord) r).getTarget().toString())
            .collect(Collectors.toSet());
        log.debug("resolvePtr.exit; returning uris: {}", set);
        return set;
    }
    
    /**
     * resolves DNS URI record
     * 
     * @param ptr - the URI record to query
     * @return the resolved Set<String> 
     */
    private Set<String> resolveUri(String uri) {
        log.debug("resolveUri.enter; got URI: {}", uri);
        Set<String> set = query(uri, Type.URI)
           	.filter(r -> r instanceof URIRecord)
            .map(r -> ((URIRecord) r).getTarget())
            .collect(Collectors.toSet());
        log.debug("resolveUri.exit; returning dids: {}", set);
        return set;
    }
	
	/**
	 * 
	 * @param uri - the domain to be queried
	 * @param type - DNS record type
	 * @return the stream of DNS records
	 * @throws IOException
	 * @throws DnsException
	 */
    private Stream<Record> query(String uri, int type) {
        if (!uri.startsWith(PREFIX)) {
            uri = PREFIX + uri;
        }
        if (!uri.endsWith(".")) {
            uri = uri + ".";
        }
        
        Record query = Record.newRecord(Name.fromConstantString(uri), type, DClass.IN);
        log.debug("query; DNS query: {}", query);
        Message response;
		try {
			response = resolver.send(Message.newQuery(query));
		} catch (IOException ex) {
            log.warn("query.error ", ex);
            return Stream.empty();
		}

        int rcode = response.getRcode();
        log.debug("query; got RCode: {} ({}); AD Flag present: {}", rcode, Rcode.string(rcode), response.getHeader().getFlag(Flags.AD));
        if (rcode != Rcode.NOERROR) {
        	// the code below is just to log an additional info about error condition
            for (RRset rrSet: response.getSectionRRsets(Section.ADDITIONAL)) {
                log.debug("query; Zone: {}", rrSet.getName());
                if (rrSet.getName().equals(Name.root) && rrSet.getType() == Type.TXT && rrSet.getDClass() == ValidatingResolver.VALIDATION_REASON_QCLASS) {
                    log.info("query; Reason: {}", ((TXTRecord) rrSet.first()).getStrings().get(0));
                }
            }
            return Stream.empty();
        }
        
        List<RRset> rrSets = response.getSectionRRsets(Section.ANSWER);
        return rrSets.stream().flatMap(s -> s.rrs().stream());
    }
    
	
    
	// TODO: to be removed, most probably
    public Collection<String> verifyIdentity(String issuer, String claim) throws Exception {
        log.debug("verifyIdentity.enter; issuer: {}, claim: {}", issuer, claim);

        TrustSchemeClaim tsClaim = new TrustSchemeClaim(claim);
        TrustSchemeFactory tsFactory = new TrustSchemeFactory();

        List<String> resp = null;
        Collection<TrustScheme> schemes = tsFactory.createTrustSchemes(tsClaim);
        if (schemes.isEmpty()) {
            throw new DnsException("Did not find TrustScheme / TrustList");
        }

        //resp.VerificationResult.FoundCorrespondingTrustScheme = scheme.getSchemeIdentifierCleaned();
        //resp.VerificationResult.TrustListDiscoveryInitiated = true;
        //resp.VerificationResult.TrustListFoundAndLoaded = scheme.getTSLlocation();

        //XMLUtil util = new XMLUtil(scheme.getTSLcontent());
        //String DID = util.getElementByXPath("//TrustServiceProviderList/TrustServiceProvider/TSPInformation/DIDName/Name[1]/text()");
        //report.addLine("DID (extracted): " + DID);
            
        resp = schemes.stream().map(s -> s.getTSLcontent()).toList();

        //if (DID.equals(issuer)) {
        //    resp.VerificationResult.FoundIssuer = issuer;
        //    resp.VerificationResult.VerifyIssuer = true;
        //    resp.VerificationResult.VerificationSuccessful = true;
        //    resp.VerificationStatus = true;
        //}
        return resp;
    }	

}
