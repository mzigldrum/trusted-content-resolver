# Build tools

- Required **JDK**: for version look into pom.xml from the root
- Required **Maven**: for version look into pom.xml from the root
- Optional **make**: tool which controls the generation of executables and other non-source files of a program from the 
  program's source files.
  On *Linux/MacOS* is available by default, for *Windows* see https://stackoverflow.com/questions/2532234/how-to-run-a-makefile-in-windows

# Steps with commands

Note: You can just execute ``make`` subcommands without make (!?) in case you don't want to use ``make`` 

- `git clone` repo, `cd` into directory

- clean target if needed

``` bash
$ make clean
mvn clean
...
```

- test

``` bash
$ make test
mvn --version   
...
mvn test -Dmaven.test.failure.ignore
...
```

- Extract used licenses into [THIRD-PARTY.txt](THIRD-PARTY.txt)

```bash
$ make third-party-txt-file
mvn install
...
rm -f THIRD-PARTY.txt
mvn license:add-third-party
...
```

# How to run java client

- build client:

``` bash
> mvn clean install
```

- run client:

``` bash
> cd target
> java -jar trusted-content-resolver-java-client-1.0.0-SNAPSHOT-full.jar <client arguments>
```

client arguments can be passed as set of key/value pairs: `k1=p1 k2=p2`. Supported parameters are: 

- `uri/u` - uri to Trusted Content Resolver (TCR) service
- `endpoint/e` - TCR method to be invoked (resolve/validate)
- `data/d` - parameters for invoked TCR method in JSON format
    - ResolveRequest: `{"issuer": "issuer1", "trustSchemePointers": ["ptr1", "ptr2"], "endpointTypes": ["eptype1", "eptype2"], "trustListServiceTypes": ["tlstype1", "tlstype2"]}`
    - ValidateRequest: `{"issuer": "issuer1", "did": "did1", "endpoints": ["endpoint1", "endpoint2"]}`
- `file/f` - file, containing parameters specified above, also in JSON format
- parameters specified via command-line arguments overwrite parameters from file

client prints invocation results to console


