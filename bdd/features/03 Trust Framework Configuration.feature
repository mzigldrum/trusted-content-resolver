Feature: Creation of trust frameworks
  creation and configuration of DIDs with well-known did configurations
  instantiation of trust lists, the envelopment of trust lists in Verifiable Credentials
  with proof and configuring the enveloped VCs in the service end point of DID Documents

  Background: fully environment setup

    Given that the Notary Connector (API) is online
    And the DNS-Server is running
    And the DID Resolver is running

  Scenario: 00014-A1_1_A create request of trust framework is successfully reflected in the DNS Zone File (200)

    Given the fully environment setup
    When the Notary sends a create request of trust framework via the Notary Connector (API)
    Then the Trust Framework is created in the Trust List Provisioning Domain
    And the Trust Framework is reflected as a PTR record in the DNS Zone Manager (Zone File 200)
    And the DID is enrolled as a URI RR mapped with corresponding Trust Framework

  Scenario: 00014-A1_2_An update request of trust framework is successfully reflected in the DNS Zone File (200)

    Given the fully environment setup
    When the Notary sends an update request of trust framework via the Notary Connector (API)
    Then the updated Trust Framework is published in the Trust List Provisioning Domain
    And the Trust Framework is reflected as a PTR record in the DNS Zone Manager (Zone File 200)
    And the DID is enrolled as a URI RR mapped with corresponding Trust Framework

  Scenario: 00014-A2_An instantiation of a trust list is reflected in the trust list storage with possibility to retrieve via API endpoints

    Given the fully environment setup
    When the Notary sends a create request of Trust List via the Notary Connector (API)
    Then a Trust List is published in storage (Web Server or IPFS) with retrievable API endpoint in the Trust List Provisioning Domain

  Scenario: 00014-A3_Creation of a Verifiable Credential (VC) is allowed with ability to sign the credential

    Given the fully environment setup
    When the DID is enrolled via the Notary Connector (API) as a URI RR mapped with corresponding Trust Framework
    Then a DID Document is created for the DID and stored on a https URL resource
    And the DID document defines a Service End Point with the URI to a VC
    Then the VC (e.g. "VC_1") is created so that it can be resolved via the URI in the DID Document
    And the "VC_1" contains the URI to resolve the Trust List
    And the "VC_1" is signed so that it can be validated with the public key from the DID Document

  Scenario: 00014-A4_A wrong context leads to an exception (400)

    Given the fully environment setup
    When the context of the Notary request is wrong
    Then the request leads to an exception (400)
    And an audit entry is created

  Scenario: 00014-A5_A missing data leads to an exception (404)

    Given the fully environment setup
    When the Notary request has some missing data
    Then the request leads to an exception (404)
    And an audit entry is created

  Scenario: 00014-A6_An error is provided if a record is in progress by the operator
 #low priority
  @manual
    Given the fully environment setup
    And an update or create record is still in progress by the operator
    When a next create/update request of trust framework is sent
    Then an error `409 Conflict` is provided

  Scenario: 00014-A7_Should be able to reference Trust Frameworks from other Domains

    Given the fully environment setup
    When a Trust Framework DNS entry (_scheme._trust.federation1.com) contains several PTR RRs (PTR RR_1,PTR RR_2,PTR RR_3)
    Then each PTR RR points to a DNS entry where the location of a trust list can be found, in a URI RR
    And the PTR RRs allows one Trust Framework to point to several trust lists from other Domains