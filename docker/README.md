## Build Procedure

Ensure you have JDK 17, Maven 3.5.4 (or newer) and Git installed

First clone the TRAIN Trusted Content Resolver (TCR) repository:

``` sh
git clone git@gitlab.eclipse.org/eclipse/xfsc/train/trusted-content-resolver.git
```

Then build project with `maven`:

``` sh
cd <tcr_root_folder>
mvn clean install 
```

## Run TCR as local Docker image
Go to `/docker` folder. Use docker compose to start universal-resolver with did:web driver only:

```sh
docker compose --env-file unires.env -f uni-resolver-web.yml up -d
```

or with all available universal-resolver drivers: 

```sh
docker compose --env-file unires.env -f uni-resolver-all.yml up -d
```

To test did:web resolution you can try the following CURL commands:

``` sh
curl -X GET http://localhost:8080/1.0/identifiers/did:web:did.actor:alice
curl -X GET http://localhost:8080/1.0/identifiers/did:web:did.actor:bob
curl -X GET http://localhost:8080/1.0/identifiers/did:web:did.actor:mike
```

Then start TCR: 
 
``` sh
docker compose up -d
```

For more information on universal-resolver please see the project on [GitHub](https://github.com/decentralized-identity/universal-resolver)
