package eu.xfsc.train.tcr.util;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.xfsc.train.tcr.api.generated.model.ResolveRequest;
import eu.xfsc.train.tcr.api.generated.model.ResolveResult;
import eu.xfsc.train.tcr.api.generated.model.ValidateRequest;
import eu.xfsc.train.tcr.api.generated.model.ValidateResponse;
import eu.xfsc.train.tcr.client.ResolveServiceClient;

public class ResolveServiceLauncher {
	
	static final String DEF_URI = "http://localhost:8087";
	static ObjectMapper mapper = new ObjectMapper();
	
	
    public static void main(String[] args) throws Exception {
    	
    	String baseUri = null;
    	String endpoint = null;
    	String data = null;
    	ResolveServiceParams params = null;
    	//System.out.println("args: " + Arrays.toString(args));
    	for (String arg: args) {
    		String[] parts = arg.split("=");
    		if ("u".equals(parts[0]) || "uri".equals(parts[0])) {
    			baseUri = parts[1];
    			continue;
    		}
    		if ("e".equals(parts[0]) || "endpoint".equals(parts[0])) {
    			endpoint = parts[1];
    			continue;
    		}
    		if ("d".equals(parts[0]) || "data".equals(parts[0])) {
    			data = parts[1];
    			continue;
    		}
    		if ("f".equals(parts[0]) || "file".equals(parts[0])) {
    			params = mapper.readValue(new File(parts[1]), ResolveServiceParams.class);
    			continue;
    		}
    		System.out.println("unknown parameter: " + arg);
    	}
    	
    	if (params != null) {
    		if (baseUri == null) {
    			baseUri = params.getUri();
    		}
    		if (endpoint == null) {
    			endpoint = params.getEndpoint();
    		}
    		if (data == null) {
    			data = params.getData();
    		}
    	}
    	if (baseUri == null) {
    		baseUri = DEF_URI;
    	}
    	
    	if (endpoint == null) {
    		System.out.println("error: no resolution endpoint specified");
    		return;
    	}
    	
    	Boolean resolve;
		if ("resolve".equals(endpoint)) {
			resolve = true;
		} else if ("validate".equals(endpoint)) {
			resolve = false;
		} else {
			System.out.println("error: unknown resolution endpoint: " + endpoint);
			return;
		}
    	
    	if (data == null) {
    		System.out.println("error: no " + (resolve ? "resolution" : "validation") + " data provided");
    		return;
    	}
    	
    	ResolveServiceClient client = new ResolveServiceClient(baseUri);
    	if (resolve) {
    		ResolveRequest rrq = mapper.readValue(data, ResolveRequest.class);
    		List<ResolveResult> result = client.resolveTrustList(rrq);
    		System.out.println("result: " + mapper.writeValueAsString(result));
    	} else {
    		ValidateRequest vrq = mapper.readValue(data, ValidateRequest.class);
    		ValidateResponse result = client.validateTrustList(vrq);
    		System.out.println("result: " + mapper.writeValueAsString(result));
    	}
    	// what else?
    }
    
}
