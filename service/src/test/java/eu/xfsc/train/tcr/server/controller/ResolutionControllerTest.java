package eu.xfsc.train.tcr.server.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.xfsc.train.tcr.api.generated.model.Error;
import eu.xfsc.train.tcr.api.generated.model.ResolveRequest;
import eu.xfsc.train.tcr.api.generated.model.ResolveResult;
import eu.xfsc.train.tcr.api.generated.model.ResolveTrustList;
import eu.xfsc.train.tcr.api.generated.model.ValidateRequest;
import eu.xfsc.train.tcr.api.generated.model.ValidateResponse;
import uniresolver.UniResolver;
import uniresolver.result.ResolveRepresentationResult;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class ResolutionControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper jsonMapper;
	@MockBean
	private UniResolver uniResolver;
	
	private static final String testDoc = """
			{"@context":["https://www.w3.org/ns/did/v1","https://w3id.org/security/suites/jws-2020/v1"],"id":"did:web:essif.trust-scheme.de","verificationMethod":[{"id":"did:web:essif.trust-scheme.de#owner","type":"JsonWebKey2020",
			"controller":"did:web:essif.trust-scheme.de","publicKeyJwk":{"kty":"OKP","crv":"Ed25519","x":"U8vPLaL0zjnRSjVwaYQNfPVW5k9j_XMF6dTxTOiAojs"}}],"service":[{"id":"did:web:essif.trust-scheme.de#issuer-list",
			"type":"issuer-list","serviceEndpoint":"https://essif.iao.fraunhofer.de/files/policy/schuelerausweis.xml"}],"authentication":["did:web:essif.trust-scheme.de#owner"],
			"assertionMethod":["did:web:essif.trust-scheme.de#owner"]}""";
	
	private static final String essifDoc = """
			{"@context":["https://www.w3.org/ns/did/v1","https://w3id.org/security/suites/jws-2020/v1"],"id":"did:web:essif.iao.fraunhofer.de","verificationMethod":[{"id":"did:web:essif.iao.fraunhofer.de#owner","type":"JsonWebKey2020",
			"controller":"did:web:essif.iao.fraunhofer.de","publicKeyJwk":{"kty":"OKP","crv":"Ed25519","x":"yaHbNw6nj4Pn3nGPHyyTqP-QHXYNJIpkA37PrIOND4c"}}],"service":[{"id":"did:web:essif.iao.fraunhofer.de#issuer-list",
			"type":"issuer-list","serviceEndpoint":"https://essif.iao.fraunhofer.de/files/trustlist/federation1.test.train.trust-scheme.de.json"}],"authentication":["did:web:essif.iao.fraunhofer.de#owner"],
			"assertionMethod":["did:web:essif.iao.fraunhofer.de#owner"]}""";
	
    private static final TypeReference<List<ResolveResult>> LIST_TYPE_REF = new TypeReference<List<ResolveResult>>() {
    };
    private static final TypeReference<Map<String, Object>> MAP_TYPE_REF = new TypeReference<Map<String, Object>>() {
    };
	

    @Test
    public void postResolveRequestShouldReturnFailureResponse() throws Exception {

    	String did1 = "did:web:essif.trust-scheme.de";
    	ResolveRepresentationResult rrr = ResolveRepresentationResult.build(null, testDoc.getBytes(), null);
        when(uniResolver.resolveRepresentation(eq(did1), any())).thenReturn(rrr);
    	String did2 = "did:web:essif.iao.fraunhofer.de";
    	rrr = ResolveRepresentationResult.build(null, essifDoc.getBytes(), null);
        when(uniResolver.resolveRepresentation(eq(did2), any())).thenReturn(rrr);
    	
        String ptr = "did-web.test.train.trust-scheme.de";
        ResolveRequest request = new ResolveRequest();
        request.setIssuer("https://test-issuer.sample.org");
        request.addEndpointTypesItem("issuer-list");
        request.addTrustSchemePointersItem(ptr); 
        
        String response = mockMvc
            .perform(MockMvcRequestBuilders.post("/resolve")
            .content(jsonMapper.writeValueAsString(request))		
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isUnprocessableEntity())
            .andReturn()
            .getResponse()
            .getContentAsString();
        Error error = jsonMapper.readValue(response, Error.class);
        assertEquals("verification_error", error.getCode());
    }

    @Test
    public void postResolveRequestShouldReturnEmptyResponse() throws Exception {
      
    	String did = "did:web:essif.trust-scheme.de";
    	ResolveRepresentationResult rrr = ResolveRepresentationResult.build(null, testDoc.getBytes(), null);
        when(uniResolver.resolveRepresentation(eq(did), any())).thenReturn(rrr);

        String ptr = "did-web.test.train.trust-scheme.de";
        ResolveRequest request = new ResolveRequest();
        request.setIssuer("https://test-issuer.sample.org");
        request.addEndpointTypesItem("wrong-type"); 
        request.addTrustSchemePointersItem(ptr);
        
        String response = mockMvc
            .perform(MockMvcRequestBuilders.post("/resolve")
            .content(jsonMapper.writeValueAsString(request))		
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();
        List<ResolveResult> result = jsonMapper.readValue(response, LIST_TYPE_REF);
        assertNotNull(result);
        assertEquals(1, result.size());
        ResolveResult rr = result.get(0);
        assertEquals(did, rr.getDid());
        assertEquals(List.of(ptr), rr.getPointers());
        assertNotNull(rr.getDocument());
        assertTrue(rr.getEndpoints().isEmpty());
    }

    @Test
    public void postResolveRequestShouldReturnTrustListResponse() throws Exception {
      
    	String did = "did:web:essif.iao.fraunhofer.de";
    	ResolveRepresentationResult rrr = ResolveRepresentationResult.build(null, essifDoc.getBytes(), null); 
        when(uniResolver.resolveRepresentation(eq(did), any())).thenReturn(rrr);
   	
        String ptr = "gxfs.test.train.trust-scheme.de";
        String issuer = "https://www.federation1.com/";
        ResolveRequest request = new ResolveRequest(issuer, List.of(ptr), null, null);
        
        String response = mockMvc
            .perform(MockMvcRequestBuilders.post("/resolve")
            .content(jsonMapper.writeValueAsString(request))		
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();
        List<ResolveResult> result = jsonMapper.readValue(response, LIST_TYPE_REF);
        assertNotNull(result);
        assertEquals(1, result.size());
        ResolveResult rr = result.get(0);
        assertEquals(did, rr.getDid());
        assertEquals(List.of(ptr), rr.getPointers());
        assertNotNull(rr.getDocument());
        assertFalse(rr.getEndpoints().isEmpty());
        ResolveTrustList rtl = rr.getEndpoints().get(0);
        assertEquals("https://essif.iao.fraunhofer.de/files/trustlist/federation1.test.train.trust-scheme.de.json", rtl.getEndpoint());
        Map<String, Object> tsp = (Map<String, Object>) rtl.getTrustList().get("tspinformation");
        Map<String, Object> tspn = (Map<String, Object>) tsp.get("tspname");
        Map<String, Object> name = ((List<Map<String, Object>>) tspn.get("name")).get(0);
        assertEquals(issuer, name.get("value"));
    }

    
    @Test
    public void postValidateRequestShouldReturnValidateResponse() throws Exception {
      
    	String did = "did:web:essif.iao.fraunhofer.de";
    	ResolveRepresentationResult rrr = ResolveRepresentationResult.build(null, essifDoc.getBytes(), null); 
        when(uniResolver.resolveRepresentation(eq(did), any())).thenReturn(rrr);
   	
        String issuer = "https://www.federation1.com/";
        ValidateRequest request = new ValidateRequest(issuer, did, //jsonMapper.readValue(essifDoc, MAP_TYPE_REF), 
        		List.of("https://essif.iao.fraunhofer.de/files/trustlist/federation1.test.train.trust-scheme.de.json"));
        
        String response = mockMvc
            .perform(MockMvcRequestBuilders.post("/validate")
            .content(jsonMapper.writeValueAsString(request))		
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();
        ValidateResponse result = jsonMapper.readValue(response, ValidateResponse.class);
        assertNotNull(result);
    }
    
}


