// TODO transfer config cucumber.js into config/cucumber.yaml

let common = [
  '../../features/**/*.feature',
  '--require-module ts-node/register',
  '--require ./test/bdd/steps/**/*.ts',
  '--format progress-bar',
  '--format node_modules/cucumber-pretty',
  // '--format html:cucumber-report.html'
].join(' ');

module.exports = {
  default: common,
  // More profiles can be added if desired
};
