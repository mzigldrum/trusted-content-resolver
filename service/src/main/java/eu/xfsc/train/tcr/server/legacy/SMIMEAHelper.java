package eu.xfsc.train.tcr.server.legacy;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.List;

import eu.xfsc.train.tcr.server.exception.DnsException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SMIMEAHelper {
	
	
	private DNSHelper dns;
	
	public SMIMEAHelper(DNSHelper dns) {
		this.dns = dns;
	}
    
    /**
     * Authenticating of a (XAdES) signed XML Document using DNSSEC / DANE.
     * Performs two steps:
     * <b>Step 1:</b> Verifies that the given Document document is signed and that the signature are valid.
     * <b>Step 2: </b> Verifies that the cert used to sign the given Document is equal to the one represented by
     * the SMIMEA records stored in the given hostname.
     *
     * @param hostname Hostname of the Document's SMIMEA records.
     * @param document The signed XML Document (e.g. a TSL or a TrustTranslation.)
     * @return <i>true</i> if all verifications succeeded.
     * @throws IOException
     */
    public boolean verifyXMLdocument(String hostname, String document) throws IOException {
        List<SMIMEAcert> smimeas = null;
        try {
            smimeas = dns.querySMIMEA(hostname);
            log.debug("verifyXMLdocument; Found {} SMIMEA record(s) for this Document", smimeas.size());
        } catch (IOException | DnsException e) {
            log.info("verifyXMLdocument.exit; Loading of SMIMEA failed.", e);
            return false;
        }
        
        if (smimeas.isEmpty()) {
            return false;
        } 

        X509Certificate signingCert = verifyDocContent(document);
        if (signingCert == null) {
            log.info("verifyXMLdocument.exit; Verification of Document signature failed.");
            return false;
        }
        
        for (SMIMEAcert smaCert: smimeas) {
            smaCert.init();
            if (verifyDocCert(signingCert, smaCert)) {
                // we found at least one record the authenticates the cert used to sign the doc
                return true;
            }
        }
            
        log.info("verifyXMLdocument.exit; Verification of Document cert failed, no chain from DNS.");
        return false;
    }
    
    
    private X509Certificate verifyDocContent(String document) {
        //XAdESHelper xades = new XAdESHelper(document, null);
        //boolean docVerificationStatus = xades.verify();
    
        //if (docVerificationStatus == false) {
        //    return null;
        //}
    
        X509Certificate signerCert = null; //xades.getCertificate();
    
        //log.info("Document signed by:   " + signerCert.getSubjectDN());
        //log.info("    Cert Fingerprint: " + Util.toString(signerCert.getFingerprintSHA()));
    
        log.info("verifyDocContent; CHECK 1/2 PASSED: Document signature valid.");
    
        return signerCert;
    }
    
    private boolean verifyDocCert(X509Certificate signingCert, SMIMEAcert record) {
        try {
            if (record.match(signingCert)) {
                log.info("verifyDocCert; CHECK 2/2 PASSED: Document is signed by cert pinned in DNS.");
                return true;
            }
        } catch (CertificateEncodingException | NoSuchAlgorithmException e) {
            log.error("verifyDocCert.error; Could not parse cert from Document", e);
        }
        return false;
    }
}
