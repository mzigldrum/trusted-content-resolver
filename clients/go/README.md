Keel here all useful information for Golang Client

To install golang interpreter see [2], base metal deployment on Mac/Ubuntu.

To set up the project [1] see usage of ``go mod download`` command, mostly targeting people new for Golang.

A good example of the Golang BDD in action can be seen in this video demo [3]


References::

[1] [Golang Environment – GOPATH vs go.mod](https://www.freecodecamp.org/news/golang-environment-gopath-vs-go-mod/)
[2] [ansible.playbook.yml](./ansible.playbook.yml)
[3] [08 - Golang - BDD cucumber tests](https://www.youtube.com/watch?v=vfTCXWyC8kc)
