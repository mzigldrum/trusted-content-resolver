package eu.xfsc.train.tcr.server.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import uniresolver.UniResolver;
import uniresolver.result.ResolveRepresentationResult;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class DIDResolverTest {

	@MockBean
	//@Autowired
	private UniResolver uniResolver;
	@Autowired
	private DIDResolver didResolver;
	
	private static final String fhDidEssif = "did:web:essif.iao.fraunhofer.de";

	private static final String fhDidTrust = "did:web:essif.trust-scheme.de";
	
	private static final String didDocSingleEP = """
			{"@context":["https://www.w3.org/ns/did/v1","https://w3id.org/security/suites/jws-2020/v1"],"id":"did:web:essif.trust-scheme.de","verificationMethod":[{"id":"did:web:essif.trust-scheme.de#owner","type":"JsonWebKey2020",
			"controller":"did:web:essif.trust-scheme.de","publicKeyJwk":{"kty":"OKP","crv":"Ed25519","x":"U8vPLaL0zjnRSjVwaYQNfPVW5k9j_XMF6dTxTOiAojs"}}],"service":[{"id":"did:web:essif.trust-scheme.de#issuer-list",
			"type":"issuer-list","serviceEndpoint":"https://essif.iao.fraunhofer.de/files/policy/schuelerausweis.xml"}],"authentication":["did:web:essif.trust-scheme.de#owner"],
			"assertionMethod":["did:web:essif.trust-scheme.de#owner"]}""";

	private static final String didDocManyEP = """
			{"@context":["https://www.w3.org/ns/did/v1","https://w3id.org/security/suites/jws-2020/v1"],"id":"did:web:essif.trust-scheme.de","verificationMethod":[{"id":"did:web:essif.trust-scheme.de#owner","type":"JsonWebKey2020",
			"controller":"did:web:essif.trust-scheme.de","publicKeyJwk":{"kty":"OKP","crv":"Ed25519","x":"U8vPLaL0zjnRSjVwaYQNfPVW5k9j_XMF6dTxTOiAojs"}}],"service":[{"id":"did:web:essif.trust-scheme.de#issuer-list",
			"type":"issuer-list","serviceEndpoint":["https://essif.iao.fraunhofer.de/firstEP", "https://essif.iao.fraunhofer.de/secondEP"]}],"authentication":["did:web:essif.trust-scheme.de#owner"],
			"assertionMethod":["did:web:essif.trust-scheme.de#owner"]}""";
	
	private static final String didDocObjectEP = """
			{"@context":["https://www.w3.org/ns/did/v1","https://w3id.org/security/suites/jws-2020/v1"],"id":"did:web:essif.trust-scheme.de","verificationMethod":[{"id":"did:web:essif.trust-scheme.de#owner","type":"JsonWebKey2020",
			"controller":"did:web:essif.trust-scheme.de","publicKeyJwk":{"kty":"OKP","crv":"Ed25519","x":"U8vPLaL0zjnRSjVwaYQNfPVW5k9j_XMF6dTxTOiAojs"}}],"service":[{"id":"did:web:essif.trust-scheme.de#issuer-list",
			"type":"issuer-list","serviceEndpoint":{"services":["https://essif.iao.fraunhofer.de/firstEP", "https://essif.iao.fraunhofer.de/secondEP"]}}],"authentication":["did:web:essif.trust-scheme.de#owner"],
			"assertionMethod":["did:web:essif.trust-scheme.de#owner"]}""";
	
	private static final String didDocKeyVC = """
			{"@context": ["https://www.w3.org/ns/did/v1", {"Ed25519VerificationKey2018": "https://w3id.org/security#Ed25519VerificationKey2018", "publicKeyJwk": {"@id": "https://w3id.org/security#publicKeyJwk", "@type": "@json"}}],
			 "id": "did:key:z6MkoTHsgNNrby8JzCNQ1iRLyW5QQ6R8Xuu6AA8igGrMVPUM", "verificationMethod": [{"id": "did:key:z6MkoTHsgNNrby8JzCNQ1iRLyW5QQ6R8Xuu6AA8igGrMVPUM#z6MkoTHsgNNrby8JzCNQ1iRLyW5QQ6R8Xuu6AA8igGrMVPUM", "type": "Ed25519VerificationKey2018",
		     "controller": "did:key:z6MkoTHsgNNrby8JzCNQ1iRLyW5QQ6R8Xuu6AA8igGrMVPUM", "publicKeyJwk": {"kty": "OKP", "crv": "Ed25519", "x": "hbtAIehGcx_wXTFzIYJzrHOwl8IGV8EzRgx__FUEnso"}}], "authentication": [
		     "did:key:z6MkoTHsgNNrby8JzCNQ1iRLyW5QQ6R8Xuu6AA8igGrMVPUM#z6MkoTHsgNNrby8JzCNQ1iRLyW5QQ6R8Xuu6AA8igGrMVPUM"], "assertionMethod": ["did:key:z6MkoTHsgNNrby8JzCNQ1iRLyW5QQ6R8Xuu6AA8igGrMVPUM#z6MkoTHsgNNrby8JzCNQ1iRLyW5QQ6R8Xuu6AA8igGrMVPUM"]}""";
	
	private static final String didDocEssif = """
			{"@context":["https://www.w3.org/ns/did/v1","https://w3id.org/security/suites/jws-2020/v1"],"id":"did:web:essif.iao.fraunhofer.de","verificationMethod":[{"id":"did:web:essif.iao.fraunhofer.de#owner","type":"JsonWebKey2020",
			"controller":"did:web:essif.iao.fraunhofer.de","publicKeyJwk":{"kty":"OKP","crv":"Ed25519","x":"yaHbNw6nj4Pn3nGPHyyTqP-QHXYNJIpkA37PrIOND4c"}}],"service":[{"id":"did:web:essif.iao.fraunhofer.de#issuer-list",
			"type":"issuer-list","serviceEndpoint":"https://essif.iao.fraunhofer.de/files/trustlist/federation1.test.train.trust-scheme.de.json"}],"authentication":["did:web:essif.iao.fraunhofer.de#owner"],
			"assertionMethod":["did:web:essif.iao.fraunhofer.de#owner"]}""";
	
    @Test
    public void testDIDResolutionSingleEP() throws Exception {
    	ResolveRepresentationResult rrr = ResolveRepresentationResult.build(null, didDocSingleEP.getBytes(), null); 
        when(uniResolver.resolveRepresentation(eq(fhDidTrust), any())).thenReturn(rrr);
    	
        DIDResolver.DIDResolveResult didRes = didResolver.resolveDid(fhDidTrust, null);
        assertNotNull(didRes);
        assertNotNull(didRes.getDocument());
        assertTrue(didRes.getEndpoints().contains("https://essif.iao.fraunhofer.de/files/policy/schuelerausweis.xml"));
    }

    @Test
    public void testDIDResolutionManyEP() throws Exception {
    	ResolveRepresentationResult rrr = ResolveRepresentationResult.build(null, didDocManyEP.getBytes(), null); 
        when(uniResolver.resolveRepresentation(eq(fhDidTrust), any())).thenReturn(rrr);
    	
        DIDResolver.DIDResolveResult didRes = didResolver.resolveDid(fhDidTrust, null);
        assertNotNull(didRes);
        assertNotNull(didRes.getDocument());
        assertEquals(2, didRes.getEndpoints().size());
        assertTrue(didRes.getEndpoints().contains("https://essif.iao.fraunhofer.de/firstEP"));
        assertTrue(didRes.getEndpoints().contains("https://essif.iao.fraunhofer.de/secondEP"));
    }
    
    @Test
    public void testDIDResolutionObjectEP() throws Exception {
    	ResolveRepresentationResult rrr = ResolveRepresentationResult.build(null, didDocObjectEP.getBytes(), null); 
        when(uniResolver.resolveRepresentation(eq(fhDidTrust), any())).thenReturn(rrr);
    	
        DIDResolver.DIDResolveResult didRes = didResolver.resolveDid(fhDidTrust, null);
        assertNotNull(didRes);
        assertNotNull(didRes.getDocument());
        assertEquals(2, didRes.getEndpoints().size());
        assertTrue(didRes.getEndpoints().contains("https://essif.iao.fraunhofer.de/firstEP"));
        assertTrue(didRes.getEndpoints().contains("https://essif.iao.fraunhofer.de/secondEP"));
    }
    
    @Test
    public void testDIDConfigResolution() throws Exception {
    	String did = "did:key:z6MkoTHsgNNrby8JzCNQ1iRLyW5QQ6R8Xuu6AA8igGrMVPUM";
    	ResolveRepresentationResult rrr = ResolveRepresentationResult.build(null, didDocKeyVC.getBytes(), null); 
        when(uniResolver.resolveRepresentation(eq(did), any())).thenReturn(rrr);
    	
    	String origin = "https://identity.foundation";
    	boolean verified = didResolver.resolveDidConfig(origin);
    	assertTrue(verified);
    }
    
    @Test
    public void testDIDAndConfigResolution() throws Exception {
    	ResolveRepresentationResult rrr = ResolveRepresentationResult.build(null, didDocEssif.getBytes(), null); 
        when(uniResolver.resolveRepresentation(eq(fhDidEssif), any())).thenReturn(rrr);
    	
        DIDResolver.DIDResolveResult didRes = didResolver.resolveDid(fhDidEssif, null);
        assertNotNull(didRes);
        assertNotNull(didRes.getDocument());
        assertEquals("https://essif.iao.fraunhofer.de", didRes.getOrigin());
        assertEquals(List.of("https://essif.iao.fraunhofer.de/files/trustlist/federation1.test.train.trust-scheme.de.json"), didRes.getEndpoints());
    	
        // now test config..
    	boolean verified = didResolver.resolveDidConfig("https://essif.iao.fraunhofer.de");
    	assertTrue(verified); 
    }
    
    @Test
    public void testTLResolution() throws Exception {
    	ResolveRepresentationResult rrr = ResolveRepresentationResult.build(null, didDocEssif.getBytes(), null); 
        when(uniResolver.resolveRepresentation(eq(fhDidEssif), any())).thenReturn(rrr);

    	DIDResolver.VCResolveResult vcRes = didResolver.resolveVC("https://essif.iao.fraunhofer.de/files/trustlist/federation1.test.train.trust-scheme.de.json");
        assertEquals("https://tspa.trust-scheme.de/tspa_train_domain/api/v1/scheme/federation1.test.train.trust-scheme.de", vcRes.getTrustListUri());
        assertTrue(vcRes.isVerified());
    }
    
}
