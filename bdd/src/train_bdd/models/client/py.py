"""
Trusted content resolver python client wrapper
"""
from typing import cast

from pathlib import Path

import bash

from ..._env import CLIENT_PY_VENV
from ..trust_framework_pointer import TrustFrameworkPointer
from ._base import BaseClient

GIT_ROOT = Path(__file__).parent.joinpath("../../../../..").resolve()


class Py(BaseClient):
    """
    BDD step implementation for Python Client
    """
    CLIENT_NAME = "trusted-content-resolver-client"

    def resolve(self, trust_framework_pointers: set[TrustFrameworkPointer], did: str) -> str:
        """
        Render example, execute them, process output for assert
        """
        template_location = GIT_ROOT / "bdd/example/clients/py/trust_content_resolver_example.py.j2"

        rendered_example = self._render_example(
            example_code=template_location.read_text(),
            trust_framework_pointers=trust_framework_pointers,
            did=did,
        )

        rendered_example_location = template_location.parent / "trust_content_resolver_example.py"

        rendered_example_location.write_text(rendered_example)

        response = bash.bash(f"'{CLIENT_PY_VENV}/bin/python' '{rendered_example_location}'")
        assert response.code == 0, response.stderr
        return cast(str, response.stdout.decode())

    def is_up(self) -> bool:
        """
        If it's available in PIP then it's installed.
        """
        output = bash.bash(f"'{CLIENT_PY_VENV}/bin/pip' list | grep {self.CLIENT_NAME}")
        if output.code != 0:
            print(f"FAILED {output.stderr}")
            return False

        return True
