<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>3.1.5</version>
        <relativePath/>
    </parent>
    
    <groupId>eu.xfsc.train</groupId>
    <artifactId>trusted-content-resolver</artifactId>
    <packaging>pom</packaging>
    <!-- Important: The property 'revision' must be updated to describe version changes in a single place for the build process -->
    <version>1.0.0.local</version>

    <name>trusted-content-resolver</name>
    <description>Eclipse XFSC TRAIN Trusted Content Resolver Project</description>
    <organization>
        <name>T-Systems International GmbH</name>
    </organization>

    <repositories>
        <repository>
            <id>danubetech-maven-public</id>
            <url>https://repo.danubetech.com/repository/maven-public/</url>
        </repository>
        <repository>
            <id>jitpack.io</id>
            <url>https://jitpack.io</url>
        </repository>
    </repositories>

    <modules>
        <module>api</module>
        <module>clients/java</module>
        <module>service</module>
    </modules>

    <properties>
        <!-- Important: The property 'revision' must be updated to describe version changes in a single place for the build process -->
        <revision>1.0.0-SNAPSHOT</revision>
        <!-- java -->
        <java.version>17</java.version>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <!-- charset -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <!-- dependencies -->
        <spring.version>6.0.13</spring.version>
        <spring-boot.version>3.1.5</spring-boot.version>
        <springdoc.version>2.2.0</springdoc.version>
        <lombok.version>1.18.30</lombok.version>
        <micrometer.version>1.11.3</micrometer.version>
        <vc.version>1.7.0</vc.version>
        <key-format.version>1.12.0</key-format.version>
        <did.version>1.11.0</did.version>
        <ld-signatures.version>1.6.0</ld-signatures.version>
        <uni-resolver.version>0.16.0</uni-resolver.version>
        <titanium.version>1.3.2</titanium.version>
        <caffeine.version>3.1.8</caffeine.version>        
        <eclipse-collections.version>11.1.0</eclipse-collections.version>
        <bcpkix.jdk15on.version>1.70</bcpkix.jdk15on.version>
        <jose4j.version>0.9.3</jose4j.version>
        <dnsjava.version>3.5.2</dnsjava.version>
        <dnssecjava.version>2.0.0</dnssecjava.version>
        <okhttp.version>4.11.0</okhttp.version>
        <dss.version>5.12.1</dss.version>
        <java-multihash.version>v1.3.4</java-multihash.version>
        <snakeyml.version>2.1</snakeyml.version>
        <xmlsec.version>2.3.4</xmlsec.version>
        <okio.version>3.6.0</okio.version>
        <jaxb.version>2.3.1</jaxb.version>
        <netty-macos.version>4.1.101.Final</netty-macos.version>
        <junit-jupiter.version>5.10.0</junit-jupiter.version>
        <junit-platform.version>1.10.0</junit-platform.version>
        <!-- plugins -->
        <plugin.jib.version>3.2.1</plugin.jib.version>
        <plugin.openapi-generator.version>6.4.0</plugin.openapi-generator.version>
        <plugin.maven-compiler.version>3.8.1</plugin.maven-compiler.version>
        <plugin.checkstyle.version>3.1.2</plugin.checkstyle.version>
        <plugin.checkstyle.puppycrawl.version>10.3.1</plugin.checkstyle.puppycrawl.version>
        <!-- repoositiry path -->
        <repository.path>eclipse/xfsc/train/trusted-content-resolver</repository.path>
        <!--jib-->
        <ci-registry>${env.CI_REGISTRY}/${repository.path}</ci-registry>
    </properties>

    <url>https://gitlab.eclipse.org/${repository.path}</url>
    <scm>
        <connection>scm:git:git://gitlab.eclipse.org/${repository.path}.git</connection>
        <url>https://gitlab.eclipse.org/${repository.path}</url>
        <tag>HEAD</tag>
    </scm>
    <issueManagement>
        <system>GitLab</system>
        <url>https://gitlab.eclipse.org/${repository.path}/-/issues</url>
    </issueManagement>
    <ciManagement>
        <url>https://gitlab.eclipse.org/${repository.path}/-/pipelines</url>
    </ciManagement>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>eu.xfsc.train</groupId>
                <artifactId>trusted-content-resolver-api</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>eu.xfsc.train</groupId>
                <artifactId>trusted-content-resolver-java-client</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-actuator</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-web</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springdoc</groupId>
                <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
                <version>${springdoc.version}</version>
            </dependency>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <optional>true</optional>
                <!--scope>provided</scope-->
                <version>${lombok.version}</version>
            </dependency>
            <dependency>
                <groupId>io.micrometer</groupId>
                <artifactId>micrometer-registry-prometheus</artifactId>
                <version>${micrometer.version}</version>
            </dependency>
            <dependency>
                <groupId>com.github.ben-manes.caffeine</groupId>
                <artifactId>caffeine</artifactId>
                <version>${caffeine.version}</version>
            </dependency> 
            <dependency>
                <groupId>com.apicatalog</groupId>
                <artifactId>titanium-json-ld</artifactId>
                <version>${titanium.version}</version>
            </dependency>
            <dependency>
                <groupId>com.danubetech</groupId>
                <artifactId>verifiable-credentials-java</artifactId>
                <version>${vc.version}</version>
            </dependency>
            <dependency>
                <groupId>com.danubetech</groupId>
                <artifactId>key-formats-java</artifactId>
                <version>${key-format.version}</version>
            </dependency>
            <dependency>
                <groupId>decentralized-identity</groupId>
                <artifactId>did-common-java</artifactId>
                <version>${did.version}</version>
            </dependency>
            <dependency>
	            <groupId>decentralized-identity</groupId>
	            <artifactId>uni-resolver-client</artifactId>
	            <version>${uni-resolver.version}</version>
            </dependency>
            <dependency>
                <groupId>info.weboftrust</groupId>
                <artifactId>ld-signatures-java</artifactId>
                <version>${ld-signatures.version}</version>
            </dependency>
            <dependency>
                <groupId>dnsjava</groupId>
                <artifactId>dnsjava</artifactId>
                <version>${dnsjava.version}</version>
            </dependency>
            <dependency>
                <groupId>org.jitsi</groupId>
                <artifactId>dnssecjava</artifactId>
                <version>${dnssecjava.version}</version>
            </dependency>
            <dependency>
                <groupId>com.squareup.okhttp3</groupId>
                <artifactId>okhttp</artifactId>
                <version>${okhttp.version}</version>
            </dependency>
            <dependency>
                <groupId>eu.europa.ec.joinup.sd-dss</groupId>
                <artifactId>dss-service</artifactId>
                <version>${dss.version}</version>
            </dependency>
            <dependency>
                <groupId>eu.europa.ec.joinup.sd-dss</groupId>
                <artifactId>dss-tsl-validation</artifactId>
                <version>${dss.version}</version>
            </dependency>
            <dependency>
                <groupId>eu.europa.ec.joinup.sd-dss</groupId>
                <artifactId>dss-utils-apache-commons</artifactId>
                <version>${dss.version}</version>
            </dependency>
            <dependency>
                <groupId>eu.europa.ec.joinup.sd-dss</groupId>
                <artifactId>dss-validation-rest</artifactId>
                <version>${dss.version}</version>
            </dependency>
            <dependency>
                <groupId>eu.europa.ec.joinup.sd-dss</groupId>
                <artifactId>dss-certificate-validation-rest</artifactId>
                <version>${dss.version}</version>
            </dependency>
            <dependency>
                <groupId>com.github.multiformats</groupId>
                <artifactId>java-multihash</artifactId>
                <version>${java-multihash.version}</version>
            </dependency>
            <dependency>
                <groupId>org.bouncycastle</groupId>
                <artifactId>bcpkix-jdk15on</artifactId>
                <version>${bcpkix.jdk15on.version}</version>
            </dependency>
            <dependency>
                <groupId>org.yaml</groupId>
                <artifactId>snakeyaml</artifactId>
                <version>${snakeyml.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.santuario</groupId>
                <artifactId>xmlsec</artifactId>
                <version>${xmlsec.version}</version>
            </dependency>
            <dependency>
                <groupId>com.squareup.okio</groupId>
                <artifactId>okio</artifactId>
                <version>${okio.version}</version>
            </dependency>
            <dependency>
                <groupId>javax.xml.bind</groupId>
                <artifactId>jaxb-api</artifactId>
                <version>${jaxb.version}</version>
            </dependency>
            <dependency>
                <groupId>org.glassfish.jaxb</groupId>
                <artifactId>jaxb-runtime</artifactId>
                <version>${jaxb.version}</version>
            </dependency>
            <dependency>
                <groupId>io.netty</groupId>
                <artifactId>netty-resolver-dns-native-macos</artifactId>
                <version>${netty-macos.version}</version>
                <classifier>osx-aarch_64</classifier>
                <scope>runtime</scope>
            </dependency>
                        
            <!-- Test dependencies -->
            <dependency>
                <groupId>org.junit.platform</groupId>
                <artifactId>junit-platform-launcher</artifactId>
                <version>${junit-platform.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter</artifactId>
                <version>${junit-jupiter.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-test</artifactId>
                <version>${spring-boot.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>com.squareup.okhttp3</groupId>
                <artifactId>mockwebserver</artifactId>
                <version>${okhttp.version}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>${plugin.checkstyle.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>com.puppycrawl.tools</groupId>
                            <artifactId>checkstyle</artifactId>
                            <version>${plugin.checkstyle.puppycrawl.version}</version>
                        </dependency>
                    </dependencies>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${plugin.maven-compiler.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>license-maven-plugin</artifactId>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                </plugin>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>${spring-boot.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.owasp</groupId>
                    <artifactId>dependency-check-maven</artifactId>
                </plugin>
                <plugin>
                    <groupId>org.openapitools</groupId>
                    <artifactId>openapi-generator-maven-plugin</artifactId>
                    <version>${plugin.openapi-generator.version}</version>
                    <configuration>
                        <generateApiTests>false</generateApiTests>
                        <generateModelTests>false</generateModelTests>
                        <generateApiDocumentation>false</generateApiDocumentation>
                        <generateModelDocumentation>false</generateModelDocumentation>
                        <configOptions>
                            <!--serializableModel>true</serializableModel-->
                            <useSwaggerUI>true</useSwaggerUI>
                            <useSpringBoot3>true</useSpringBoot3>
                        </configOptions>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>com.google.cloud.tools</groupId>
                    <artifactId>jib-maven-plugin</artifactId>
                    <version>${plugin.jib.version}</version>
                    <configuration>
                        <container>
                            <!-- ${maven.build.timestamp} should work also -->
                            <creationTime>USE_CURRENT_TIMESTAMP</creationTime>
                        </container>						
                        <!-- Turn it off for all the modules, but
                            turn it on in server module only -->
                        <skip>true</skip>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <annotationProcessorPaths>
                        <path>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                            <version>${lombok.version}</version>
                        </path>
                    </annotationProcessorPaths>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <configuration>
                    <includes>**/*.java</includes>
                    <copyrightOwners>${project.organization.name} and all other contributors</copyrightOwners>
                    <processStartTag>---license-start</processStartTag>
                    <processEndTag>---license-end</processEndTag>
                    <sectionDelimiter>---</sectionDelimiter>
                    <addJavaLicenseAfterPackage>false</addJavaLicenseAfterPackage>
                    <trimHeaderLine>true</trimHeaderLine>
                    <emptyLineAfterHeader>true</emptyLineAfterHeader>
                    <outputDirectory>.</outputDirectory>
                    <thirdPartyFilename>THIRD-PARTY.txt</thirdPartyFilename>
                    <!--fileTemplate>templates/third-party.ftl</fileTemplate-->
                    <!--excludedArtifacts>dgc-lib</excludedArtifacts-->
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
