package eu.xfsc.train.tcr.server.service;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.europa.esig.trustedlist.jaxb.tsl.TrustStatusListType;
import eu.europa.esig.trustedlist.jaxb.tsl.TSPType;
import eu.xfsc.train.tcr.api.generated.model.ResolveRequest;
import eu.xfsc.train.tcr.api.generated.model.ResolveResult;
import eu.xfsc.train.tcr.api.generated.model.ResolveTrustList;
import eu.xfsc.train.tcr.api.generated.model.ValidateRequest;
import eu.xfsc.train.tcr.api.generated.model.ValidateResponse;
import eu.xfsc.train.tcr.server.generated.controller.TrustedContentResolverApiDelegate;
import eu.xfsc.train.tcr.server.service.DIDResolver.DIDResolveResult;
import eu.xfsc.train.tcr.server.service.DIDResolver.VCResolveResult;
import eu.xfsc.train.tcr.server.service.TLResolver.TLResolveResult;
import lombok.extern.slf4j.Slf4j;

/**
 * Implementation of the
 * {@link eu.xfsc.train.tcr.server.generated.controller.TrustedContentResolverApiDelegate}
 * interface.
 */
@Slf4j
@Service
public class ResolutionService implements TrustedContentResolverApiDelegate {

	private static final TypeReference<Map<String, Object>> MAP_TYPE_REF = new TypeReference<Map<String, Object>>() {
	};

	@Autowired
	private DNSResolver reDns;
	@Autowired
	private DIDResolver reDid;
	@Autowired
	private TLResolver reTL;
	@Autowired
	private ObjectMapper jsonMapper;

	@Override
	public ResponseEntity<List<ResolveResult>> resolveTrustList(ResolveRequest resolveRequest) {
		log.debug("resolveTrustList.enter; got request: {}", resolveRequest);
		Map<String, ResolveResult> result = new HashMap<>();
		resolveRequest.getTrustSchemePointers().forEach(ptr -> {
			Collection<String> dids = null;
			try {
				dids = reDns.resolveDomain(ptr);
			} catch (Exception ex) {
				log.warn("resolveTrustList.error", ex);
				// add error block to response?
			}
			if (dids != null) {
				dids.forEach(did -> {
					ResolveResult rr = result.computeIfAbsent(did, s -> new ResolveResult().did(s));
					if (rr.getPointers().contains(ptr)) {
						log.debug("resolveTrustList; DID {} for PTR {} has been already resolved", did, ptr);
					} else {
						rr.addPointersItem(ptr);
						DIDResolveResult didRes = reDid.resolveDid(did, resolveRequest.getEndpointTypes());
						if (didRes.getOrigin() != null) {
							// here we perform well-known did-configuration check..
							if (!reDid.resolveDidConfig(didRes.getOrigin())) {
								// should we remove the not-verified entry?
								log.info("resolveTrustList; resolved origin not verified: {}", didRes.getOrigin());
							}
						}
						rr.setDocument(didRes.getDocument());
						didRes.getEndpoints().forEach(endpoint -> {
							ResolveTrustList rtl = new ResolveTrustList().endpoint(endpoint);
							VCResolveResult vcRes = reDid.resolveVC(endpoint);
							//reTL.validateDocument(vcRes.getTrustListUri());
							TLResolveResult tl = reTL.resolveTLHash(vcRes.getTrustListUri(), vcRes.getHash()); 
							if (tl != null) {
								Optional<TSPType> tsPro = findIssuerProvider(tl.getTrustList(), resolveRequest.getIssuer());
								if (tsPro.isPresent()) {
									rtl.setTrustList(convertToMap(tsPro.get()));
								}
							}
							rr.addEndpointsItem(rtl);
						});
					}
				});
			}
		});
		log.debug("resolveTrustList.exit; returning result: {}", result);
		return ResponseEntity.ok(result.values().stream().toList());
	}

	@Override
    public ResponseEntity<ValidateResponse> validateTrustList(ValidateRequest validateRequest) {
		log.debug("validateTrustList.enter; got request: {}", validateRequest);
		ValidateResponse result = new ValidateResponse();
		DIDResolveResult didRes = reDid.resolveDid(validateRequest.getDid(), null);
		if (didRes.getOrigin() == null) {
			result.setDidVerified(false);
			log.info("validateTrustList; origin not resolved");
		} else {
			// here we perform well-known did-configuration check..
			result.setDidVerified(reDid.resolveDidConfig(didRes.getOrigin()));
			if (!result.getDidVerified()) {
				log.info("validateTrustList; resolved origin not verified: {}", didRes.getOrigin());
			}
		}
		validateRequest.getEndpoints().forEach(endpoint -> {
			ResolveTrustList rtl = new ResolveTrustList().endpoint(endpoint);
			VCResolveResult vcRes = reDid.resolveVC(endpoint);
			result.setVcVerified(vcRes.isVerified());
			if (vcRes.getTrustListUri() != null) {
				TrustStatusListType tl = reTL.resolveTL(vcRes.getTrustListUri(), false);
				if (tl != null) {
					Optional<TSPType> tsPro = findIssuerProvider(tl, validateRequest.getIssuer());
					if (tsPro.isPresent()) {
						rtl.setTrustList(convertToMap(tsPro.get()));
						result.setIssuerVerified(true);
						result.addEndpointsItem(rtl);
					}
				}
			}
			
		});
		log.debug("validateTrustList.exit; returning result: {}", result);
		return ResponseEntity.ok(result);
	}
	
	private Optional<TSPType> findIssuerProvider(TrustStatusListType trustList, String issuer) {
		return trustList.getTrustServiceProviderList().getTrustServiceProvider().stream()
				.filter(tsp -> tsp.getTSPInformation().getTSPName().getName().stream()
						.anyMatch(n -> issuer.equals(n.getValue()))).findFirst();
	}
	

	private Map<String, Object> convertToMap(TSPType tsp) {
		try {
			byte[] json = jsonMapper.writeValueAsBytes(tsp);
			return jsonMapper.readValue(json, MAP_TYPE_REF);
		} catch (IOException ex) {
			log.warn("convertToMap.error; ", ex);
			return null;
		}
	}

}
