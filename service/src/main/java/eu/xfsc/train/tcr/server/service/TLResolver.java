package eu.xfsc.train.tcr.server.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.OptionalInt;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.europa.esig.dss.enumerations.MimeType;
import eu.europa.esig.dss.enumerations.MimeTypeEnum;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.service.crl.OnlineCRLSource;
import eu.europa.esig.dss.service.ocsp.OnlineOCSPSource;
import eu.europa.esig.dss.service.http.commons.CommonsDataLoader;
import eu.europa.esig.dss.service.http.commons.FileCacheDataLoader;
import eu.europa.esig.dss.spi.client.http.DSSFileLoader;
import eu.europa.esig.dss.spi.client.http.IgnoreDataLoader;
import eu.europa.esig.dss.spi.tsl.TrustedListsCertificateSource;
import eu.europa.esig.dss.spi.x509.CertificateSource;
import eu.europa.esig.dss.spi.x509.KeyStoreCertificateSource;
import eu.europa.esig.dss.spi.x509.aia.DefaultAIASource;
import eu.europa.esig.dss.tsl.cache.CacheCleaner;
import eu.europa.esig.dss.tsl.function.OfficialJournalSchemeInformationURI;
import eu.europa.esig.dss.tsl.job.TLValidationJob;
import eu.europa.esig.dss.tsl.source.LOTLSource;
import eu.europa.esig.dss.tsl.source.TLSource;
import eu.europa.esig.dss.tsl.sync.AcceptAllStrategy;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.validation.SignedDocumentValidator;
import eu.europa.esig.dss.validation.reports.Reports;
import eu.europa.esig.trustedlist.TrustedListFacade;
import eu.europa.esig.trustedlist.jaxb.tsl.TrustStatusListType;
import io.ipfs.multihash.Multihash;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TLResolver {
	
	// Should be externalized
	private static final String LOTL_URL = "https://ec.europa.eu/tools/lotl/eu-lotl.xml";
	private static final String OJ_URL = "https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.C_.2019.276.01.0001.01.ENG";
	
	private final TrustedListFacade facade = TrustedListFacade.newFacade();
	
	@Autowired
	private ObjectMapper jsonMapper;
	
	public TrustStatusListType resolveTL(String uri) {
		return resolveTL(uri, false);
	}
	
	public TrustStatusListType resolveTL(String uri, boolean validate) {
		log.debug("resolveTL.enter; got uri: {}, validate: {}", uri, validate);
		
		TrustStatusListType trustList = null;
		try {
			String content = resolveContent(uri);
			trustList = parseContent(content, validate);
		} catch (IOException | JAXBException | XMLStreamException | SAXException ex) {
			log.error("resolveTL.error;", ex); 
		}
		log.debug("resolveTL.exit; returning TL: {}", trustList);
		return trustList;
	}
	
	public TLResolveResult resolveTLHash(String uri, String hash) {
		log.debug("resolveTLHash.enter; got uri: {}, hash: {}", uri, hash);

		String content = null;
		TrustStatusListType trustList = null;
		try {
			content = resolveContent(uri);
			trustList = parseContent(content, false);
		} catch (Exception ex) {
			log.warn("resolveTLHash.error reading TrustList", ex);
		}
		if (trustList == null) {
			return null;
		}
        
		TLResolveResult tlResult = new TLResolveResult(trustList, null, false);
		try {
			Multihash vcHash = Multihash.fromBase58(hash); // .decode(hash); 
			Multihash.Type mType = vcHash.getType();

			String algo = getDigestType(mType);
			log.debug("resolveTLHash; hash algo is: {}", algo);
			MessageDigest md = MessageDigest.getInstance(algo);
			md.update(content.getBytes()); 
			Multihash tlHash = new Multihash(mType, md.digest());
			tlResult.setTlHash(tlHash.toString());
			tlResult.setHashVerified(vcHash.equals(tlHash));
		} catch (Exception ex) {
			log.warn("resolveTLHash.error verifying Hash: {}", ex.getMessage());
		}
		log.debug("resolveTLHash.exit; returning: {}", tlResult);
		return tlResult;
	}
	
	private String resolveContent(String uri) throws IOException {
		DSSFileLoader dl = onlineLoader();
		DSSDocument tlDoc = dl.getDocument(uri);
		log.debug("resolveContent; got TL doc with type: {}", tlDoc.getMimeType());
		InputStream input = tlDoc.openStream();
		return new String(input.readAllBytes(), StandardCharsets.UTF_8);
	}
	
	private TrustStatusListType parseContent(String content, boolean validate) throws JAXBException, XMLStreamException, IOException, SAXException {
		MimeType type = getDocType(content);
		if (type == MimeTypeEnum.XML) {
			return facade.unmarshall(content, validate);
		} 
		if (type == MimeTypeEnum.JSON) {
			return jsonMapper.readValue(content, TrustStatusListType.class);
		} 
		log.info("parseContent; got content with unknown type: {}", type);
		return null;
	}

	private MimeType getDocType(String content) {
		OptionalInt opt = content.chars().filter(c -> c == '<' || c == '{').findFirst();
		if (opt.isPresent()) {
			if (opt.getAsInt() == '<') {
				return MimeTypeEnum.XML;
			} else if (opt.getAsInt() == '{') {
				return MimeTypeEnum.JSON;
			}
		}
		return MimeTypeEnum.BINARY;
	}
	
	private String getDigestType(Multihash.Type mType) throws NoSuchAlgorithmException {
		switch (mType) {
			case sha1: return "SHA-1";
			case sha2_256: return "SHA-256";
			case sha2_512: return "SHA-512";
			case sha3_256: return "SHA3-256";
		}
		throw new NoSuchAlgorithmException("Unsupported MType: " + mType);
	}
	
	public boolean validateTL(String path) {
		log.debug("validateTL.enter; got path: {}", path);
		TLSource tlSource = new TLSource();
		tlSource.setUrl(path);
		TLValidationJob tlvJob = new TLValidationJob();
		tlvJob.setTrustedListSources(tlSource);
		tlvJob.setOfflineDataLoader(offlineLoader());
		tlvJob.offlineRefresh();

		tlvJob.setOnlineDataLoader(onlineLoader());
		tlvJob.onlineRefresh();
		log.debug("validateTL.exit; summary: {}", tlvJob.getSummary());
		return true;
	}
	
	public void validateDocument(String path) {
		log.debug("validateDocument.enter got path: {}", path);
		CommonCertificateVerifier commonCertificateVerifier = new CommonCertificateVerifier();
		TLValidationJob job = job();
		TrustedListsCertificateSource trustedListsCertificateSource = new TrustedListsCertificateSource();
		job.setTrustedListCertificateSource(trustedListsCertificateSource);
		job.onlineRefresh();
		commonCertificateVerifier.setTrustedCertSources(trustedListsCertificateSource);
		commonCertificateVerifier.setCrlSource(new OnlineCRLSource());
		commonCertificateVerifier.setOcspSource(new OnlineOCSPSource());
		commonCertificateVerifier.setAIASource(new DefaultAIASource());
		
		SignedDocumentValidator validator = SignedDocumentValidator.fromDocument(new FileDocument(path));
		validator.setCertificateVerifier(commonCertificateVerifier);
		Reports repo = validator.validateDocument();
		log.debug("validateDocument.exit; report: {}", repo.getXmlSimpleReport());
	}


	private TLValidationJob job() {
		TLValidationJob job = new TLValidationJob();
		job.setOfflineDataLoader(offlineLoader());
		job.setOnlineDataLoader(onlineLoader());
		job.setTrustedListCertificateSource(trustedCertificateSource());
		job.setSynchronizationStrategy(new AcceptAllStrategy());
		job.setCacheCleaner(cacheCleaner());

		LOTLSource europeanLOTL = europeanLOTL();
		job.setListOfTrustedListSources(europeanLOTL);

		//job.setLOTLAlerts(Arrays.asList(ojUrlAlert(europeanLOTL), lotlLocationAlert(europeanLOTL)));
		//job.setTLAlerts(Arrays.asList(tlSigningAlert(), tlExpirationDetection()));

		return job;
	}

	private TrustedListsCertificateSource trustedCertificateSource() {
		return new TrustedListsCertificateSource();
	}

	private LOTLSource europeanLOTL() {
		LOTLSource lotlSource = new LOTLSource();
		lotlSource.setUrl(LOTL_URL);
		//lotlSource.setCertificateSource(officialJournalContentKeyStore());
		lotlSource.setSigningCertificatesAnnouncementPredicate(new OfficialJournalSchemeInformationURI(OJ_URL));
		lotlSource.setPivotSupport(true);
		return lotlSource;
	}

	private CertificateSource officialJournalContentKeyStore() {
		try {
			return new KeyStoreCertificateSource(new File("src/main/resources/keystore.p12"), "PKCS12", "dss-password");
		} catch (IOException e) {
			throw new DSSException("Unable to load the keystore", e);
		}
	}

	private DSSFileLoader offlineLoader() {
		FileCacheDataLoader offlineFileLoader = new FileCacheDataLoader();
		offlineFileLoader.setCacheExpirationTime(-1); // negative value means cache never expires
		offlineFileLoader.setDataLoader(new IgnoreDataLoader());
		offlineFileLoader.setFileCacheDirectory(tlCacheDirectory());
		return offlineFileLoader;
	}

	private DSSFileLoader onlineLoader() {
		FileCacheDataLoader onlineFileLoader = new FileCacheDataLoader();
		onlineFileLoader.setCacheExpirationTime(0);
		onlineFileLoader.setDataLoader(new CommonsDataLoader()); // instance of DataLoader which can access to Internet (proxy,...)
		onlineFileLoader.setFileCacheDirectory(tlCacheDirectory());
		return onlineFileLoader;
	}

	private File tlCacheDirectory() {
		File rootFolder = new File(System.getProperty("java.io.tmpdir"));
		File tslCache = new File(rootFolder, "dss-tsl-loader");
		if (tslCache.mkdirs()) {
			log.info("TL Cache folder : {}", tslCache.getAbsolutePath());
		}
		return tslCache;
	}

	private CacheCleaner cacheCleaner() {
		CacheCleaner cacheCleaner = new CacheCleaner();
		cacheCleaner.setCleanMemory(true);
		cacheCleaner.setCleanFileSystem(true);
		cacheCleaner.setDSSFileLoader(offlineLoader());
		return cacheCleaner;
	}
	
	@Getter
	@Setter
	@AllArgsConstructor
	@ToString
	class TLResolveResult {
		
		private TrustStatusListType trustList;
		private String tlHash;
		private boolean hashVerified;
		
	}
		
	
}
