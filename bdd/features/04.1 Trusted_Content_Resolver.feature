Feature: Testing Trusted Content Resolver REST API
  This functionality MUST allow for the resolution of the trust list to find the issuer details in the trust list.

  Background: Interfaces data interfaces running
    Given TCR is running
      And DID Resolver is running

  @succeed
  Scenario Outline: Trust Discovery with <Authoritative DNS server> succeed

    Given client <Client> installed
      And <Authoritative DNS server> with <IP> is running
      And multiple Trust Framework Pointers
          """
          sausweis.train1.trust-scheme.de
          sausweis.train2.trust-scheme.de
          """
      And trust framework pointers are `configured` in DNS
      And Validate DNS Name against DNSSEC
    When above Trust Framework Pointers are supplied in resolver trust request by `did:example:123456789abcdefghijk`
    Then Trust List's corresponding Trust Framework pointers from context

    Examples: Combination "DNS Server" / "Client implementation"
      | Authoritative DNS server | IP      | Client                                       |
      | Configured Mocked DNS    | x.x.x.1 | Trust-content-resolver-client-validator-go   |
      | Configured Mocked DNS    | x.x.x.1 | Trust-content-resolver-client-validator-java |
      | Configured Mocked DNS    | x.x.x.1 | Trust-content-resolver-client-validator-js   |
      | Configured Mocked DNS    | x.x.x.1 | Trust-content-resolver-client-validator-py   |

      #| CI/CD KNOT               | x.x.x.2 |
      #| CI/CD NSD                | x.x.x.3 |
      #| Fraunhofer NSD           | 3.67.18.47 |


  @edge-case
  Scenario Outline: Trust Discovery with <Authoritative DNS server> fail

    Given <Authoritative DNS server> with <IP> is running
      And multiple Trust Framework Pointers
          """
          sausweis.train3.trust-scheme.de
          sausweis.train4.trust-scheme.de
          """
      And trust framework pointers are `misconfigured` in DNS
     When above Trust Framework Pointers are supplied in resolver trust request by `did:example:123456789abcdefghijk`
     Then Trust List's will be emtpy

    Examples: DNS Server
      | Authoritative DNS server | IP      |
      | Misconfigured Mocked DNS | x.x.x.1 |
