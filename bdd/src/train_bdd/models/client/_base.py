"""All the client will have the same checks"""
from typing import Any

import abc
from abc import ABC
from textwrap import dedent

import jinja2
from jinja2.nativetypes import NativeEnvironment


class BaseClient(ABC):  # pylint: disable=too-few-public-methods
    """
    Base Class for java, py, go, js, ...
    """
    @classmethod
    def _render_example(cls, example_code: str, *args: Any, **kwargs: Any) -> str:
        """
        Render code template into real executable source code to be used as example.
        """
        env = NativeEnvironment(undefined=jinja2.StrictUndefined)

        template = env.from_string(dedent(example_code).strip() + "\n\n")

        return template.render(*args, **kwargs)

    @abc.abstractmethod
    def is_up(self) -> bool:
        """Check if client is installed"""
